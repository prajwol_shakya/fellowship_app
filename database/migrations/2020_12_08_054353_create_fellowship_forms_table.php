<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFellowshipFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fellowship_forms', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->unsigned();
            $table->string('mobile_no')->nullable();
            $table->string('full_name')->nullable();
            $table->string('father_name')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('nationality')->nullable();
            $table->string('citizenship_no')->nullable();
            $table->date('citizenship_date')->nullable();
            $table->string('citizenship_district')->nullable();
            $table->date('dob')->nullable();
            $table->string('permanent_address')->nullable();
            $table->string('temporary_address')->nullable();
            $table->string('ug_degree')->nullable();
            $table->string('degree_from')->nullable();
            $table->string('ug_college_name')->nullable();
            $table->date('nmc_reg_date')->nullable();
            $table->string('nmc_reg_no')->nullable();
            $table->string('pg_degree')->nullable();
            $table->string('pg_degree_from')->nullable();
            $table->string('pg_college_name')->nullable();
            $table->string('pg_marks_obtained')->nullable();
            $table->string('sq_exam_detail')->nullable();
            $table->string('affiliated')->nullable();
            $table->string('insitute_hospital')->nullable();
            $table->string('position_head')->nullable();
            $table->string('experience')->nullable();
            $table->integer('form_no')->default(0);
            $table->integer('payment_approved')->default(0);
            $table->integer('application_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fellowship_forms');
    }
}
