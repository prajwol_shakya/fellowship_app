<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsSubmitToFellowshipFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fellowship_forms', function (Blueprint $table) {
            //
            $table->integer('is_submit')->unsigned()->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fellowship_forms', function (Blueprint $table) {
            //
            $table->dropColumn('is_submit');

        });
    }
}
