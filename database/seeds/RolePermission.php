<?php

use Illuminate\Database\Seeder;

class RolePermission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $config = config('laratrust_seeder.role_structure');
        $userPermission = config('laratrust_seeder.permission_structure');
        $mapPermission = collect(config('laratrust_seeder.permissions_map'));

        foreach ($config as $key => $modules) {

            // Create a new role
            $role = \App\Role::updateOrCreate(
                ['name' => $key],
                [
                'display_name' => ucwords(str_replace('_', ' ', $key)),
                'description' => ucwords(str_replace('_', ' ', $key))
            ]);
           

            $this->command->info('Creating Role '. strtoupper($key));
            if($key=='user'){
                $user_type = 2;
            }else{
                $user_type = 1;
            }
            if($key == 'superadmin')
           { 
            $this->command->info("Creating '{$key}' user");
               // Create superadmin
                $user = \App\User::updateOrCreate(
                  [  'name' => ucwords(str_replace('_', ' ', $key))],
                  [
                    'email' => $key.'@bpk.com',
                    'password' => bcrypt('password'),
                    'user_type' => $user_type
                  ]);

                $user->attachRole($role);
            }
            // // Reading role permission modules
            // foreach ($modules as $module => $value) {

            //     foreach (explode(',', $value) as $p => $perm) {

            //         $permissionValue = $mapPermission->get($perm);

            //         $permissions[] = \App\Permission::firstOrCreate([
            //             'name' => $permissionValue . '-' . $module,
            //             'display_name' => ucfirst($permissionValue) . ' ' . ucfirst($module),
            //             'description' => ucfirst($permissionValue) . ' ' . ucfirst($module),
            //         ])->id;

            //         $this->command->info('Creating Permission to '.$permissionValue.' for '. $module);
            //     }
            // }
        }
    }
}
