<?php

use Illuminate\Database\Seeder;

class PaymentMethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Seeding...');
        $details=[
                [
                'name'=>'Cash',
                'status'=>'0',
                ],
                [
                'name'=>'Esewa',
                'status'=>'1',
                ],
                [
                'name'=>'Khalti',
                'status'=>'0',
                ],
                [
                'name'=>'Bank Voucher',
                'status'=>'1',
                ],
        		 ];
        foreach ($details as $key => $value) {
	        DB::table('payment_methods')->insert([
              'name'=>$value['name'], 
              'status'  => $value['status'],
              'created_at' => now(),
              'updated_at' => now()
	        ]);
        }
        $this->command->info('Seeding Completed!');
    }
}
