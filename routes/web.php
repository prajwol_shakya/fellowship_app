<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', function () {
	if(Auth::check()) {
		if((Auth::user()->user_type)==2){
			return redirect()->route('user.form');
		}else{
			return redirect()->route('list.forms');
		}
    }else{
		return redirect()->route('register');
	}
});

// Route::get('/phpinfo', function () {
// phpinfo();
// });
// Route::get('/', 'Auth\RegisterController@showRegistrationForm')->name('home');

Auth::routes();


Route::get('/register', 'HomeController@forms')->name('register');
Route::post('/set-cookies', 'HomeController@setCookies');
Route::post('/user-register', 'Auth\RegisterController@registerUser')->name('user.register');
Route::get('verifyaccount/{code}','HomeController@checkcode');



Route::group(['middleware' => ['auth', 'IsAdmin']], function () {

Route::get('/fellowship-forms-list', 'FellowshipFormController@index')->name('list.forms');
Route::get('/fellowship-forms-list', 'FellowshipFormController@index')->name('list.forms');
Route::post('/upload-image', 'FellowshipFormController@uploadImage')->name('image');


Route::post('/create-fellowship', 'FellowshipFormController@create')->name('create.forms');
Route::get('/form-list', 'FellowshipFormController@formList')->name('form.list');
Route::post('/update-fellowship/{id}', 'FellowshipFormController@update')->name('update.forms');
Route::post('/form-delete', 'FellowshipFormController@delete')->name('delete.forms');

Route::get('/user-management', 'UsermanagementController@index')->name('user.list');
Route::post('/create-user', 'UsermanagementController@create')->name('user.create');
Route::get('/get-user-list', 'UsermanagementController@userlist')->name('list.users');
Route::post('/update-user/{id}', 'UsermanagementController@update')->name('update.user');
Route::post('/user-delete', 'UsermanagementController@delete')->name('delete.user');
Route::post('/update-user-role/{id}', 'UsermanagementController@updateRole')->name('update.user');

Route::post('/admin-preview-file', 'BackendController@previewfile')->name('preview.file');
Route::get('/user-applied-form-list/{form_id}', 'BackendController@userAppliedFormList')->name('userAppliedFormList');

// Route::get('/dashboard', 'BackendController@dashboard')->name('dashboard');
Route::get('/fellowship-form/{form_id}', 'BackendController@fellowshipforms')->name('fellowshipform.data');
Route::post('/update-appliccation-status', 'BackendController@updateAppStatus')->name('app.status');
Route::post('/update-payment-status', 'BackendController@updatepaymentStatus')->name('payment.status');
Route::post('/get-user-application', 'BackendController@getuserApplication')->name('get.user.app');
});


Route::group(['middleware' => ['auth', 'IsUser']], function () {
	Route::get('/payment_failure','PaymentController@failure')->name('payment_failure');
	Route::get('/payment_successful','PaymentController@esewaPaySuccessful')->name('payment_successful');
// Route::get('/user-dashboard', 'HomeController@index')->name('home');

Route::get('/home/{form}', 'HomeController@index')->name('home');
Route::get('/user/form-list', 'HomeController@userforms')->name('user.form');
Route::post('/check-user-form', 'HomeController@checkform')->name('check.form');

Route::post('/store-fellowship-form', 'HomeController@personalStore')->name('user.personal');
Route::post('/store-fellowship-form-payment', 'HomeController@payTransaction');
Route::post('/fellowship-document-store', 'HomeController@image')->name('image');
Route::post('/fellowship-document-destroy', 'HomeController@documentDestroy')->name('image.destroy');
Route::get('/get-fellowship-document', 'HomeController@getDocument')->name('image.get');
Route::get('/preview/{form}', 'HomeController@preview')->name('preview');
Route::post('/edit-form-no', 'HomeController@editFormNo')->name('preview');
Route::post('/preview-file', 'HomeController@previewfile')->name('previewfile');
Route::get('/success-preview/{slug}', 'HomeController@successPreview')->name('success.preview');

Route::post('/form-submit', 'HomeController@formSubmit')->name('form.submit');
Route::post('/get-user-detail', 'HomeController@getUserForm');
Route::post('/get-user-detail', 'HomeController@getUserForm');



Route::get('/payment/{form}', 'PaymentController@payment')->name('payment');
Route::get('/user-dashboard', 'PaymentController@history')->name('user.dashboard');
Route::get('/payment-history-list', 'PaymentController@historyList')->name('history.list');
Route::post('/payment-voucher', 'PaymentController@paymentVoucher')->name('payment.voucher');
Route::get('payment_verify','PaymentController@esewaPaymentVerify');
Route::post('store-bank-voucher','PaymentController@payByBankVoucher')->name('save.voucher');
});









