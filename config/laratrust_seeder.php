<?php

return [
    'role_structure' => [
        'mavorion' => [
            'users' => 'c,r,u,d',
            'acl' => 'r',
            'report' => 'c',
            'setup'=> 'c,r,u,d'
        ],
        'superadmin' => [
            'users' => 'c,r,u,d',
            'acl' => 'r',
            'report' => 'c',
            'setup'=> 'c,r,u,d'
        ],
        'administrator' => [
            'users' => 'c,r,u,d',
            'acl' => 'r',
            'report' => 'c',
            'setup'=>'c,r,u,d'
        ],
        'user' => [
        ]
    ],
    'permission_structure' => [],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];

