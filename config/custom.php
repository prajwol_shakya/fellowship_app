<?php

return [
'app_url' => env('MIX_APP_URL', 'http://localhost'),
'app_env'=>env('APP_ENV','local'),
'esewa_url' => env('MIX_ESEWA_URL', 'https:'),
'esewa_code' => env('MIX_ESEWA_SERVICE_CODE', 'EPAYTEST'),
'esewa_transc_url' => env('MIX_ESEWA_TRANSC_URL', 'https:'),
'ime_url' => env('MIX_IME_URL', 'https://stg.imepay.com.np:7979/WebCheckout/Checkout'),
'merchant_code' => env('MIX_IME_MERCHANT_CODE', 'https://uat.esewa.com.np/epay/transrec'),
'apiuser' => env('MIX_IME_API_USER', 'https://uat.esewa.com.np/epay/transrec'),
'password' => env('MIX_IME_PASSWORD', 'https://uat.esewa.com.np/epay/transrec'),
'module' => env('MIX_IME_MODULE', 'https://uat.esewa.com.np/epay/transrec'),
'ime_token' => env('MIX_IME_TOKEN_URL', 'https://uat.esewa.com.np/epay/transrec'),
'ime_confirm' => env('MIX_IME_CONFIRM_URL', 'https://uat.esewa.com.np/epay/transrec'),
'ime_recheck' => env('MIX_IME_RECHECK_URL', 'https://uat.esewa.com.np/epay/transrec'),
'form_price' => 100
];