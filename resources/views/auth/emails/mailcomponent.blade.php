<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8" />
    <title>BP Koirala FellowShip Application</title>
    {{-- <link href="{{ asset('css/style.css') }}" rel="stylesheet" /> --}}
    </head>
    <body>
    <div id="app">

    <div style="width:70%;background:rgb(239,243,248);margin:0 auto;padding:8px 15px;text-align:center;">
        <img src="{{ asset('logo') }}/favicon.png" style="width: 5%">
        {{-- <!-- <img src="{{$image_path}}" alt=""> --> --}}
    </div>

        @yield('content')

    <div style="width:70%;background:rgb(239,243,248);margin:0 auto;padding:8px 15px">
        {{-- <p>Copyright © Central Zoo</a> | All rights reserved. </p> --}}
    </div>
    </div>
    </body>
</html>