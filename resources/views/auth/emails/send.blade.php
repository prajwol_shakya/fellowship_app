@extends('auth.emails.mailcomponent')
@section('content')
<div style="width:50%;border:1px solid rgb(239,243,248);margin:0 auto;padding:8px 14px">
<div class="col-lg-12 mx-auto">
<span style="color:#333333;font:14px/1.25em 'Helvetica Neue',Arial,Helvetica;font-weight:bold;line-height:20px;">Dear {{$user->name}},</span><br>
​
<br>
​
<p class="text-success sitecolor">{!!$message!!}</p>
<br>
<span style="color:#333333;font:14px/1.25em 'Helvetica Neue',Arial,Helvetica;font-weight:bold;line-height:20px;">Regards,<br>
    BP Koirala FellowShip Team</span>
</div>
</div>
@endsection