@extends('layouts.app')
@push('style')
    <style>
        .fellowship-programs .card-title {
            margin: 30px 30px -15px 30px;
        }

        .card.col-md-10.col-12.fellowship-programs {
            color: #333;
            margin-bottom: 30px;

        }
        .login
    {
        text-align: center;
    background: #8a4e94;
    margin-top: -4%;
    padding: 5%;
    }
    .login-btn
    {
        color: #fff !important;
    background-color: #204d74 !important;
    border-color: #122b40 !important;
    }
    .register-top
    {
        margin-top: 19px;
    }

    </style>
@endpush
@section('content')
<div class="login">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                {{-- <div class="card card-custom" id="register"> --}}
                    {{-- <div ><h1>Login</h1></div> --}}
                    @if ($errors->any())
                    <div class="alert alert-danger alert-flash">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="fa fa-close"></i>
                    </button>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                    <div class="form-group row">
    
                        <div class="col-md-5">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i>
                                  </div>
                                </div>
                            <input id="email" placeholder="Enter your email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                            {{-- @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror     --}}
                        </div>
                          
                        </div>
                        <div class="col-md-5">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i class="fa fa-lock" aria-hidden="true"></i>

                                  </div>
                                </div>
                            <input id="password" placeholder="Enter your password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                            {{-- @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror     --}}
                        </div>
                          
                        </div>
                            <div class="col-md-2 ">
                                <button type="submit" class="btn btn-primary login-btn">
                                    {{ __('Login') }}
                                </button>
                            </div>;
    
                    {{-- </div> --}}
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row justify-content-center register-top">
        <div class="col-md-8">
            <div class="card card-custom" id="register">
                <div class="card-header"><h1>Register</h1></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('user.register') }}">
                        @csrf
                        <div class="form-group row">
                        <label for="name" class="col-md-2 col-form-label text-md-left">{{ __('Full Name :') }}</label>

<div class="col-md-10">
    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

    @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-2 col-form-label text-md-center">{{ __('E-Mail Address :') }}</label>

                            <div class="col-md-10">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="dob" class="col-md-2 dob col-form-label text-md-left">{{ __('Date of birth :') }}</label>

                            <div class="col-md-4">
                                <input id="dob" type="date" class="form-control @error('dob') is-invalid @enderror" name="dob" required >

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <label for="password" class="col-md-2 col-form-label text-md-left">{{ __('Nationality :') }}</label>

                            <div class="col-md-4">
                               <select class="form-control" name="nationality">
                               <option value="null">Select nationality</option>
                              @foreach ($countries as $country)
                              <option {{ $country->country_name == old('nationality', 'Nepal') ? "selected='selected'" : '' }}
                                value="{{ $country->country_name }}">{{$country->country_name }}</option>
                              @endforeach
                               </select>
                                @error('nationality')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-2 col-form-label text-md-left">{{ __('Password :') }}</label>

                            <div class="col-md-4">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        
                            <label for="password-confirm" class="col-md-2 col-form-label text-md-left">{{ __('Confirm Password :') }}</label>

                            <div class="col-md-4">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                        <div class="form-group row reg-info">
                    <p><i class="fas fa-info-circle"></i> Already a user? Login or by clicking Register, you agree to the Privacy Statement</p>    
                    </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-12 reg-button">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid info-box">
    <div class="row">
        <div class="content-box col-md-9">
            <h1>Choose A FellowShip Program</h1>
            <p>&nbsp;</p>
            @foreach ($forms as $form)
                <div class="card col-md-10 col-12 fellowship-programs">
                    <div class="card-content">
                        <div class="card-title">
                            <h2>{{ $form->name }}</h2>

                            <hr>
                            <h6>Application Deadline: {{ $form->to }}</h6>
                        </div>
                        <div class="card-body">
                            <p>
                                {{ $form->description }}
                            </p>
                            <h6>Registration Cost: {{ $form->amount }}</h6>
                            <p>&nbsp;</p>
                            <button class="btn btn-primary check" data-slug="{{ $form->slug }}" onclick="setcookies('{{ $form->slug }}')">Apply</button>
                        </div>
                    </div>

                </div>
            @endforeach

        </div>
    </div>
</div>
<div class="container">
    <div class="content-box col-md-9">
    <h1>Contact Us</h1>
    <p>&nbsp;</p>
    <div class="col-md-12 col-xs-12">
    <p><i class="fas fa-info-circle"></i> To apply for a fellowship, please Register to the Fellowship System.
    For event queries or technical queries about this site, please use the information below.</p>
    <div class="col-md-12">
    <p>&nbsp;</p>
    <div class="row col-12">
    
    <div class="text-md-left col-md-2 col-6"><h4><i class="fas fa-phone"></i> Phone : </h4></div><div class="col-md-10 col-6">056-524501</div>
    </div>
    <div class="row col-12">
    <div class="col-md-2 text-md-left col-6"><h4><i class="fas fa-fax"></i> Fax&nbsp;&nbsp; : </h4></div><div class="col-md-6 col-6">056-523747</div>
    </div>
    <div class="row col-12">
    <div class="col-md-2 text-md-left col-6"><h4><i class="fas fa-envelope-open-text"></i> Email : </h4></div><div class="col-md-10 col-6">info@bpk.com</div>
    </div>
    </div>
    </div>
    </div>
    </div>
@endsection

@push('script')
<script>
    $(document).ready(function() {
      
        $(document).on("click","input[type='password']", function(){             
     var elem = document.querySelector(".alert");
        elem.style.display = "none";
        var error = document.querySelector(".invalid-feedback");
        error.style.display = "none";
        $("input[type='email']").removeClass('is-invalid');

       
})


$(document).on("click","input[type='email']", function(){             
     var elem = document.querySelector(".alert");
        elem.style.display = "none";
        var error = document.querySelector(".invalid-feedback");
        error.style.display = "none";
        $("input[type='email']").removeClass('is-invalid');

})


     
    });
    function setcookies(slug) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            console.log();
            $.ajax({
                url: '/set-cookies',
                type: 'POST',
                dataType: "json",
                data: {
                    'slug': slug
                },
                success: function(data) {
                    if (data.status == 200) {
                        window.location.href = "/login";
                    } 
                    else
                    {
                        Swal.fire({
                                        icon: 'error',
                                        title: data.message,
                                        confirmButtonText: `ok`,

                                    });
                    }

                },
                error: function(jqXHR, textStatus, errorThrown) {

                }
            });
        }
</script>
@endpush

