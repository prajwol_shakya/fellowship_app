<div class="container">

@foreach($mocks  as $mock)
@if(in_array($mock['type'],['png','jpg','jpeg']))
<div>
    <a href="{{ $mock['url'] }}" data-fancybox="images" data-caption="">
        <img src="{{ $mock['url'] }}" style="width: 100%;min-height: 470px;"/>
        </a>
</div>

@elseif(in_array($mock['type'],['pdf']))
<div class="pdf">
    <a data-fancybox data-type="iframe" data-src="{{ $mock['url'] }}" href="javascript:;">
        open {{ $mock['type'] }}
    </a>
</div>
@elseif(in_array($mock['type'],['html']))
<div class="pdf">
    <a src="{{ $mock['url'] }}" href="javascript:;" class="download" data-src="{{ $mock['url'] }}">
        Download
    </a>
</div>
@elseif(in_array($mock['type'],['docx','doc']))
<div class="file">
<a class="word" href="https://docs.google.com/gview?url={{ url($mock['url']) }}&embedded=true">Open a Word document</a>
</div>
@else
<h5>NO Document</h5>
@endif
@endforeach
</div>
<style>
.word
{
    margin:20px;
}

.file{

    margin-top: 6%;
    margin-left: 35%;


}
.pdf{

    margin-top: 6%;
    margin-left: 44%;
}
</style>
<script>



    $(document).ready(function() {
 $(".word").fancybox({
    toolbar  : false,
	smallBtn : true,
	iframe : {
		preload : true
	}
 });


$( ".download" ).click(function() {
   var src = $(this).data('src');
   downloadURI(src);
});
}); //  ready 

function downloadURI(uri, name='') 
{
        // var mammoth = require("mammoth");
console.log(uri)
    mammoth.convertToHtml({path: uri})
    .then(function(result){
        var html = result.value; // The generated HTML 
        console.log(html,'ss')
        var messages = result.messages; // Any messages, such as warnings during conversion 
    })
    .done();
    // var link = document.createElement("a");
    // // If you don't know the name or want to use
    // // the webserver default set name = ''
    // link.setAttribute('download', name);
    // link.href = uri;
    // document.body.appendChild(link);
    // link.click();
    // link.remove();
}
    </script>