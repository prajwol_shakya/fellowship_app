@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="modal fade bd-example-modal-lg" id="myModel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header mymodel-header">
                <h5 class="modal-title " id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body mymodel">
                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
          </div>
        </div>
      </div>
      <div class="modal fade bd-example-modal-lg" id="myfile" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header myfile-header">
                <h5 class="modal-title " id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body myfile">
                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
          </div>
        </div>
      </div>
    <table class="table table-bordered" id="payment-table">
        <thead>
            <tr>
                <th>Fellowship form name</th>
                <th>Invoice No</th>
                {{-- <th>Mobile No</th> --}}
                <th>Payment Status</th>
                <th>Application Status</th>
                <th>Action</th>

            </tr>
        </thead>
    </table>
    </div>
</div>
@endsection

@push('script')
<script>

     $(document).on("click", ".detail", function (ev) {
         var  form_id = $(this).data('form_id');

        $.ajaxSetup({
         headers: {
         'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
         }
         });
         console.log();
         $.ajax({
             url: '/get-user-detail',
             type: 'POST',
             dataType: "json",
             data:{'form_id':form_id},

             success: function (data) {
                 $('.mymodel').empty().append(data.data);
                 $(".mymodel-header > h5").empty().text("Applicant Form Detail"); 
                 $('#myModel').modal('show');

             },
             error: function (jqXHR, textStatus, errorThrown) {
                 // Empty most of the time...
             }
         });
});
$( "body" ).click(function() {
 
 if( !$('#myfile').hasClass('show') ) {
     $('#myModel').css('overflow-y', 'auto');

}

});
$(function() {
    $('#payment-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('history.list') !!}',
        
        columns: [
            { data: 'form_name', name: 'form_name' },
            { data: 'inv_id', name: 'inv_id' },
            // { data: 'mobile_no', name: 'mobile_no' },
            { data: 'payment_status', name: 'payment_status' },
            { data: 'app_status', name: 'app_status' },
            { data: 'action', name: 'action' }
        ]
    });
    });
</script>
@endpush
