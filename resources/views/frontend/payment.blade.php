@extends('layouts.app')
@push('style')
<style>
    .voucher-bank
    {
        width: 250px;
    }
    .voucher-btn
    {
        margin-left: 44%;
    }
    .esewa-clickable
    {
        cursor: pointer;
        width: 20%;

        
    }
    .online
    {
        margin-left: 14%;
    margin-top: 12%;
    }

    @media (max-width: 768px){
          img.online_gateway_logo {
    max-width: 135px;
    margin-bottom: 10px;
    margin-left: 10px;
    }

    h3.payment_or {
    margin: 5% 0px;
    }

    .panel-body.paybody {
    margin-top: 25px;
    }

    .online-pay form {
    margin-bottom: 15px;
}
    }
   

   @media(min-width:778px){
h3.payment_or {
margin-top:80%;
}

.online-pay form {
    margin-bottom: 15px;
    display: inline-flex;
}
   }
</style>
    
@endpush
@section('content')
    <div class="container">
        <div class="content-body">
            <div class="row">
                <div class="col-sm-4">
                <h2>Summary</h2>
                <hr>
                
                <table class="table table-striped">
                <thead>
                <tr>
                <th>Name</th>
                <th>Total</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                <td>{{  $form->name }} </td>
                <td>{{ $pay->total }}</td>
                </tr>
                <tr>
                <td><h2>Total</h2> </td>
                <td>{{  $pay->total }}</td>
                </tr>
                </tbody>
                </table>
                </div>
                <div class="col-sm">
                <div class="panel-body paybody">
                <div class="buy-info card-heading">
                <h2> Choose your payment method</h2>
                <hr>
                </div>
                <div>
                <div class="row">
                <div class="col-sm-7 online-pay">
                <h5 class="voucher" "="">Online Payment</h5>
                <form>
                <div>
                <input type="radio" name="payment" value="esewa"> <img src="/logo/esewa.jpg" class="online_gateway_logo">
                </div><div>
                <input type="radio" name="payment" value="khalti"> <img src="/logo/khalti.jpg" class="online_gateway_logo">
                </div><div>
                <input type="radio" name="payment" value="imepay"> <img src="/logo/imepay.jpg" class="online_gateway_logo">
                </div>
                </form>
                <button class="btn btn-primary pay"> Continue</button>
                <p class="payment_info">
                <i class="fas fa-info-circle"></i> If you encounter any issues with payment gateways like Esewa, Khalti, IME-Pay etc, please contact respective gateways.
                </p>
                
                </div>
                <div class="col-sm-1">
                <h3 class="payment_or">OR</h3>
                </div>
                <div class="col-sm-3">
                <h5 class="voucher">Bank Voucher</h5>
                {{-- <form method="post" action="" enctype="multipart/form-data" id="myform"> --}}
                <input type="file" name="voucher" class="form-control voucher-bank" placeholder="bank"  accept = "application/pdf,image/jpeg,image/png,image/jpg">
                <button class="btn btn-success voucher-btn" onclick="submitVoucher()">Upload</button>
                <p class="payment_info">
                <i class="fas fa-info-circle"></i>
                Bank Vouchers will need to be verified by the respective adminstration.
                </p>
                {{-- </form> --}}
                
                </div>
                </div>
                <div class="col-lg-12 col-md-12 nopadding">
                
                
                
                </div>
                
                </div></div>
                </div>
                </div>
        </div>
    @endsection

    @push('script')
        <script>
              $(document).ready(function() {
            $( ".pay" ).click(function() {
                var val = $("input[name='payment']:checked").val();
                if(val=='esewa')
                {
                    post();
                }
                else
                {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Coming Soon',
                        confirmButtonText: 'Close',
                        // text: 'Comming Soon'
                        })
                }

            });
              });
            function post() {
                var path = "https://uat.esewa.com.np/epay/main";
                var params = {
                    amt: {!! $pay->total !!},
                    psc: 0,
                    pdc: 0,
                    txAmt: 0,
                    tAmt: {!! $pay->total !!},
                    pid: '{!!  $pay->inv_id !!}',
                    scd: "{{ Config::get('custom.esewa_code') }}",
                    su: "{{ Config::get('custom.app_url') }}/payment_verify?q=su",
                    fu: "{{ Config::get('custom.app_url') }}/payment_failure?q=fu"
                }
                var form = document.createElement("form");
                form.setAttribute("method", "POST");
                form.setAttribute("action", path);

                for (var key in params) {
                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", key);
                    hiddenField.setAttribute("value", params[key]);
                    form.appendChild(hiddenField);
                }
                document.body.appendChild(form);
                form.submit();
            }
            function openmodel()
            {
                $('#myModel').modal('show');
            }

            function submitVoucher() {
                var fd = new FormData();
                var files = $('.voucher-bank')[0].files[0];
                fd.append('file', files);
                fd.append('total', {!! $pay-> total !!});
                fd.append('inv_id', '{!!  $pay->inv_id !!}');

                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Are you sure?',
                    // text: "You want to upload bank Voucher!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Confirm',
                    cancelButtonText: 'Cancel',
                    reverseButtons: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        savevoucher(fd);
                    } 
                })
            }

            function savevoucher(fd) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '/store-bank-voucher',
                    type: 'POST',
                    dataType: "json",
                    contentType: false,
                    processData: false,
                    data: fd,
                    success: function(data) {
                        if (data.status === 200) {
                            window.location.href = "/success-preview/{!! $form->slug !!}";
                        } else if(data.status == 500)
                        {
                            Swal.fire({
                                title: 'Please choose file',
                                showDenyButton: false,
                                showCancelButton: false,
                                confirmButtonText: `Cancel`,
                                })
                        }
                        else {
                            window.location.href = "/payment_failure";
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // Empty most of the time...
                    }
                });

            }

        </script>
    @endpush
