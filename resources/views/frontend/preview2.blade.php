@extends('layouts.app')
@section('style')
    <style>
        #heading {
            text-transform: uppercase;
            color: #673AB7;
            font-weight: normal
        }

        .form-card {
            text-align: left;
            margin-left: 10%;

        }

        .form-group label {
            float: left;
        }

        #msform fieldset:not(:first-of-type) {
            display: none
        }


        #msform .action-button {
            width: 100px;
            background: #673AB7;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 5px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 30px 10px 5px;
            float: right;
        }

        #msform .action-button:hover,
        #msform .action-button:focus {
            background-color: #311B92
        }

        #msform .action-button-previous {
            width: 100px;
            background: #616161;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 5px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 5px 10px 0px;
            float: right
        }

        #msform .action-button-save {
            width: 100px;
            background: #28a745;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 5px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 5px 10px 0px;
            float: right
        }

        #msform .action-button-previous:hover,
        #msform .action-button-previous:focus {
            background-color: #000000
        }

        #msform .action-button-save:hover,
        #msform .action-button-save:focus {
            background-color: #0d3c18
        }

        .card {
            z-index: 0;
            border: none;
            position: relative
        }

        .fs-title {
            font-size: 25px;
            color: #673AB7;
            margin-bottom: 15px;
            font-weight: normal;
            text-align: left
        }

        .purple-text {
            color: #673AB7;
            font-weight: normal
        }

        .steps {
            color: gray;
            margin-bottom: 10px;
            font-weight: normal;
            text-align: right
        }

        /* .fieldlabels {
                                        color: gray;
                                        text-align: left
                                    } */

        #progressbar {
            margin-bottom: 30px;
            overflow: hidden;
            color: lightgrey
        }

        #progressbar .active {
            color: #673AB7
        }

        #progressbar li {
            list-style-type: none;
            font-size: 15px;
            width: 18%;
            float: left;
            position: relative;
            font-weight: 400
        }

        #progressbar #account:before {
            font-family: FontAwesome;
            content: "\f13e"
        }

        #progressbar #personal:before {
            font-family: FontAwesome;
            content: "\f007"
        }

        #progressbar #payment:before {
            font-family: FontAwesome;
            content: "\f030"
        }

        #progressbar #confirm:before {
            font-family: FontAwesome;
            content: "\f00c"
        }

        #progressbar li:before {
            width: 50px;
            height: 50px;
            line-height: 45px;
            display: block;
            font-size: 20px;
            color: #ffffff;
            background: lightgray;
            border-radius: 50%;
            margin: 0 auto 10px auto;
            padding: 2px
        }

        #progressbar li:after {
            content: '';
            width: 100%;
            height: 2px;
            background: lightgray;
            position: absolute;
            left: 0;
            top: 25px;
            z-index: -1
        }

        #progressbar li.active:before,
        #progressbar li.active:after {
            background: #673AB7
        }

        .progress {
            height: 14px;
            box-shadow: inset 1px 2px 1px #21252924;
        }

        .progress-bar {
            background-color: #673AB7
        }

        .fit-image {
            width: 100%;
            object-fit: cover
        }

        label.error {
            color: red;
        }
        .dz-image > img{
            width: 75%;
            height: 111px;
        }
        .proceed-left
        {
            float: left;
        }
        .doc-view
        {
            margin-left: 50%;
        }
        .btn-width
        {
            width: 223px;
        }
        .view-file
        {
            background-color: #4c96e7 !important;
            border-color: #4c96e7 !important;
        }

    </style>
@endsection
@section('content')
    <div class="container col-md-8">
       
        <div class="modal fade bd-example-modal-lg" id="myModel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Online Payment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
              </div>
            </div>
          </div>
        <div class="card card-custom row justify-content-center">
            <div class="alert alert-success" role="alert">
                Congratulation Your transaction has been successful.
              </div>
            <div class="text-center p-0 mt-3 mb-2">
                <div class="px-0 pt-4 pb-0 mt-3 mb-3">
                    <fieldset class="form6" id="msform">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title" style="text-align:end;">Preview</h2>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Personal Information</h2>
                                </div>
                            </div>
                            <div class="row">
                                {{-- <input class="form-control" type="hidden" name="form_no" value="5" /> --}}
                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Full Name: *</label>
                                    <input disabled class="form-control" id="form6_full_name" type="text" name="full_name"
                                        placeholder="Full Name" value="{{ old('full_name', $userdata->full_name) }}" />
                                </div>

                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Father Name: *</label>
                                    <input disabled class="form-control" type="text" name="father_name" placeholder="Father Name"
                                        value="{{ old('father_name', $userdata->father_name) }}" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Mother Name: *</label>
                                    <input disabled class="form-control" type="text" name="mother_name" placeholder="Mother Name"
                                        value="{{ old('mother_name', $userdata->mother_name) }}" />
                                </div>
                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Nationality: *</label>
                                    <input class="form-control" type="text" disabled
                                        name="nationality" placeholder="Nationality"
                                        value="{{ old('nationality', $userdata->nationality) }}" />
                                   
                                    {{-- <select disabled class="form-control" name="nationality">
                                        <option  value="null">Select nationality</option>
                                        @foreach ($countries as $country)
                                            <option
                                                {{ $country->country_name == old('nationality', $userdata->nationality) ? "selected='selected'" : '' }}
                                                value="{{ $country->country_name }}">{{ $country->country_name }}
                                            </option>
                                        @endforeach
                                    </select> --}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Citizenship No: *</label>
                                    <input disabled class="form-control" type="text" name="citizenship_no"
                                        placeholder="Citizenship no"
                                        value="{{ old('citizenship_no', $userdata->citizenship_no) }}" />
                                </div>

                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Citizenship Taken District.: *</label>
                                    <input disabled class="form-control" type="text" name="citizenship_district"
                                        placeholder="Citizenship Taken District."
                                        value="{{ old('citizenship_district', $userdata->citizenship_district) }}" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Citizenship Date.: *</label>
                                    <input  disabled class="form-control" type="date" name="citizenship_date"
                                        placeholder="Citizenship Date."
                                        value="{{ old('citizenship_date', $userdata->citizenship_date) }}" />
                                </div>
                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Date of birth.: *</label>
                                    <input disabled class="form-control" type="date" name="dob" placeholder="Date of birth."
                                        value="{{ old('dob', $userdata->dob) }}" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2 form-group">
                                    <button type="button" class="btn btn-primary edit" data-form="0">Edit</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Contact Information:</h2>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Email: *</label>
                                    <input disabled class="form-control" type="text" name="email" placeholder="Email"
                                        value="{{ old('email', $userdata->email) }}" disabled />
                                </div>

                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Mobile No: *</label>
                                    <input  disabled class="form-control" type="text" name="mobile_no" placeholder="Mobile No"
                                        value="{{ old('mobile_no', $userdata->mobile_no) }}" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Correspondence Address: *</label>
                                    <input disabled class="form-control" type="text" name="temporary_address"
                                        placeholder="Correspondence Address"
                                        value="{{ old('temporary_address', $userdata->temporary_address) }}" />
                                </div>

                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Permanent Address: *</label>
                                    <input disabled class="form-control" type="text" name="permanent_address"
                                        placeholder="Permanent Address"
                                        value="{{ old('permanent_address', $userdata->permanent_address) }}" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2 form-group">
                                    <button type="button" class="btn btn-primary edit" data-form="1">Edit</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">UG Qualification Detail:</h2>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Degree Name: *</label>
                                    <input disabled class="form-control" type="text" name="ug_degree" placeholder="Degree Name"
                                        value="{{ old('ug_degree', $userdata->ug_degree) }}" />
                                </div>

                                <div class="col-md-6 col-12 form-group">

                                    <label class="fieldlabels">Degree From: *</label>
                                    <select disabled class="form-control" id="cars" name="degree_from"
                                        value="{{ old('degree_from', $userdata->degree_from) }}">
                                        <option value="Nepali University">Nepali University</option>
                                        <option value="Foreign University">Foreign University</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">College Name: *</label>
                                    <input disabled class="form-control" type="text" name="ug_college_name"
                                        placeholder="Collage Name"
                                        value="{{ old('ug_college_name', $userdata->ug_college_name) }}" />
                                </div>

                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Nepal Medical Council Reg Date.: *</label>
                                    <input  disabled class="form-control" type="date" name="nmc_reg_date"
                                        placeholder="Nepal Medical Council Reg Date."
                                        value="{{ old('nmc_reg_date', $userdata->nmc_reg_date) }}" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Nepal Medical Council Reg No.: *</label>
                                    <input disabled class="form-control" type="text" name="nmc_reg_no"
                                        placeholder="Nepal Medical Council Reg No."
                                        value="{{ old('nmc_reg_no', $userdata->nmc_reg_no) }}" />
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-2 form-group">
                                    <button type="button" class="btn btn-primary edit" data-form="2">Edit</button>
                                </div>
                                <div class="col-4 form-group doc-view" >
                                    <button type="button" class="btn btn-secondary view-file" data-form="2">View files</button>
                                </div>
                            </div>
                            {{-- <div class="row">
                                <div class="col-12 form-group">
                                    <label class="fieldlabels">upload UG Qualification document:*</label>
                                    <div class="needsclick dropzone" id="ugdocument-dropzone">

                                    </div>
                                </div>
                            </div> --}}
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">PG Qualification Detail:</h2>
                                </div>
                            </div>
                            <div class="row">
                                <input class="form-control" type="hidden" name="form_no" value="3" />

                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Degree Name: *</label>
                                    <input disabled class="form-control" type="text" name="pg_degree" placeholder="Degree Name"
                                        value="{{ old('pg_degree', $userdata->pg_degree) }}" />
                                </div>

                                <div class="col-md-6 col-12 form-group">

                                    <label class="fieldlabels">Degree From: *</label>
                                    <select disabled class="form-control" id="cars" name="pg_degree_from"
                                        value="{{ old('pg_degree_from', $userdata->pg_degree_from) }}">
                                        <option value="Nepali University">Nepali University</option>
                                        <option value="Foreign University">Foreign University</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">College Name: *</label>
                                    <input disabled class="form-control" type="text" name="pg_college_name"
                                        placeholder="Collage Name"
                                        value="{{ old('pg_college_name', $userdata->pg_college_name) }}" />
                                </div>

                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Mark Obtained.: *</label>
                                    <input disabled class="form-control" type="text" name="pg_marks_obtained"
                                        placeholder="Mark Obtained."
                                        value="{{ old('pg_marks_obtained', $userdata->pg_marks_obtained) }}" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Specialist Qualifying Exam Detail.: *</label>
                                    <input disabled class="form-control" type="text" name="sq_exam_detail"
                                        placeholder="Specialist Qualifying Exam Detail."
                                        value="{{ old('sq_exam_detail', $userdata->sq_exam_detail) }}" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2 form-group">
                                    <button type="button" class="btn btn-primary edit" data-form="3">Edit</button>
                                </div>
                                <div class="col-4 form-group doc-view" >
                                    <button type="button" class="btn btn-secondary view-file" data-form="3">View files</button>
                                </div>
                            </div>
                            {{-- <div class="row">
                                <div class="col-12 form-group">
                                    <label class="fieldlabels">upload pg Qualification document:*</label>
                                    <div class="needsclick dropzone" id="pgdocument-dropzone">

                                    </div>
                                </div>
                            </div> --}}
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Experience Detail:</h2>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Affiliated: *</label>
                                    <input disabled class="form-control" type="text" name="affiliated" placeholder="affiliated"
                                        value="{{ old('affiliated', $userdata->affiliated) }}" />
                                </div>

                                <div class="col-md-6 col-12 form-group">

                                    <label class="fieldlabels">Insitute/Hospital: *</label>
                                    <input disabled class="form-control" type="text" name="insitute_hospital"
                                        placeholder="Insitute/Hospital"
                                        value="{{ old('insitute_hospital', $userdata->insitute_hospital) }}" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Position Head.: *</label>
                                    <input disabled class="form-control" type="text" name="position_head"
                                        placeholder="Position Head."
                                        value="{{ old('position_head', $userdata->position_head) }}" />
                                </div>

                                <div class="col-md-6 col-12 form-group">
                                    <label class="fieldlabels">Period Of Experence.: *</label>
                                    <input disabled class="form-control" type="text" name="experience"
                                        placeholder="Period Of Experience."
                                        value="{{ old('experience', $userdata->experience) }}" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2 form-group">
                                    <button type="button" class="btn btn-primary edit" data-form="4">Edit</button>
                                </div>
                                <div class="col-4 form-group doc-view" >
                                    <button type="button" class="btn btn-secondary view-file" data-form="4">View files</button>
                                </div>
                                {{-- <div class="col-5 form-group">
                                    <button type="button" class="btn btn-success success">Proceed for payment</button>
                                </div> --}}
                            </div>
                            {{-- <div class="row proceed-left col-md-12">
                                <div class="col-md-12 form-group">
                                
                                <hr><button type="button" class="btn btn-success success btn-width proceed">Proceed for payment</button>
                                </div>
                                </div> --}}
                        </div>

                        <input type="button" name="next" class="submit action-button" value="Submit" />
                        {{-- <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> --}}
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>
    <script>
        $(document).ready(function() {
            $( ".action-button" ).click(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                console.log();
                $.ajax({
                    url: '/form-submit',
                    type: 'POST',
                    dataType: "json",
                  
                    success: function(data) {
                        Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Application successfully submitted',
                                showConfirmButton: false,
                                timer: 1500
                            })
                             .then((value) => {
                                
                                window.location.href= "/user-dashboard";
                                
                            });
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                       
                    }
                });
               
            });
        });
           

    
    </script>
@endpush

