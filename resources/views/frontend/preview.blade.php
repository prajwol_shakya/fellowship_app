@extends('layouts.app')
@section('style')
    <style>
        #heading {
            text-transform: uppercase;
            color: #673AB7;
            font-weight: normal
        }

        .form-card {
            text-align: left;
            margin-left: 10%;

        }

        .form-group label {
            float: left;
        }

        #msform fieldset:not(:first-of-type) {
            display: none
        }


        #msform .action-button {
            width: 100px;
            background: #673AB7;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 5px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 30px 10px 5px;
            float: right;
        }

        #msform .action-button:hover,
        #msform .action-button:focus {
            background-color: #311B92
        }

        #msform .action-button-previous {
            width: 100px;
            background: #616161;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 5px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 5px 10px 0px;
            float: right
        }

        #msform .action-button-save {
            width: 100px;
            background: #28a745;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 5px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 5px 10px 0px;
            float: right
        }

        #msform .action-button-previous:hover,
        #msform .action-button-previous:focus {
            background-color: #000000
        }

        #msform .action-button-save:hover,
        #msform .action-button-save:focus {
            background-color: #0d3c18
        }

        .card {
            z-index: 0;
            border: none;
            position: relative
        }

        .fs-title {
            font-size: 25px;
            color: #673AB7;
            margin-bottom: 15px;
            font-weight: normal;
            text-align: left
        }

        .purple-text {
            color: #673AB7;
            font-weight: normal
        }

        .steps {
            color: gray;
            margin-bottom: 10px;
            font-weight: normal;
            text-align: right
        }

        /* .fieldlabels {
                                        color: gray;
                                        text-align: left
                                    } */

        #progressbar {
            margin-bottom: 30px;
            overflow: hidden;
            color: lightgrey
        }

        #progressbar .active {
            color: #673AB7
        }

        #progressbar li {
            list-style-type: none;
            font-size: 15px;
            width: 18%;
            float: left;
            position: relative;
            font-weight: 400
        }

        #progressbar #account:before {
            font-family: FontAwesome;
            content: "\f13e"
        }

        #progressbar #personal:before {
            font-family: FontAwesome;
            content: "\f007"
        }

        #progressbar #payment:before {
            font-family: FontAwesome;
            content: "\f030"
        }

        #progressbar #confirm:before {
            font-family: FontAwesome;
            content: "\f00c"
        }

        #progressbar li:before {
            width: 50px;
            height: 50px;
            line-height: 45px;
            display: block;
            font-size: 20px;
            color: #ffffff;
            background: lightgray;
            border-radius: 50%;
            margin: 0 auto 10px auto;
            padding: 2px
        }

        #progressbar li:after {
            content: '';
            width: 100%;
            height: 2px;
            background: lightgray;
            position: absolute;
            left: 0;
            top: 25px;
            z-index: -1
        }

        #progressbar li.active:before,
        #progressbar li.active:after {
            background: #673AB7
        }

        .progress {
            height: 14px;
            box-shadow: inset 1px 2px 1px #21252924;
        }

        .progress-bar {
            background-color: #673AB7
        }

        .fit-image {
            width: 100%;
            object-fit: cover
        }

        label.error {
            color: red;
        }
        .dz-image > img{
            width: 75%;
            height: 111px;
        }
        .proceed-left
        {
            float: left;
        }
        .doc-view
        {
            margin-left: 50%;
        }
        .btn-width
        {
            width: 223px;
        }
        .view-file
        {
            background-color: #4c96e7 !important;
            border-color: #4c96e7 !important;
        }
#theImg
{
    height: 450px;
    width: 750px;

}
.form-group
{
    text-align: justify;
}

    </style>
@endsection
@section('content')
    <div class="container col-md-8">
       
        <div class="modal fade bd-example-modal-lg" id="myModel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Online Payment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
              </div>
            </div>
          </div>
        <div class="card card-custom row justify-content-center">
            <div class="text-center p-0 mt-3 mb-2">
                <div class="px-0 pt-4 pb-0 mt-3 mb-3">
                    <fieldset class="form6" id="msform">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-7">
                                    @if($status == 0)
                                    <h2 class="fs-title" style="text-align:center;margin-left: 46%;">Preview</h2>
                                    @else
                                    <h2 class="fs-title" style="text-align:center;margin-left: 46%;">Overview</h2>
                                    @endif

                                </div>

                            </div>
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Personal Information</h2>
                                </div>
                            </div>
                            <div class="row">
                                {{-- <input class="form-control" type="hidden" name="form_no" value="5" /> --}}
                                <div class="col-md-5 col-12 form-group">
                                    Full Name : {{ $userdata->full_name }}
                                   
                                        
                                </div>

                                <div class="col-md-5 col-12 form-group">
                                   
                                        Father Name : {{ $userdata->father_name }}

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-12 form-group">
                                    
                                        Mother Name : {{ $userdata->mother_name }}

                                </div>
                                <div class="col-md-5 col-12 form-group">
                                   
                                    Nationality : {{ $userdata->nationality }}

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-12 form-group">
                                   
                                        Citizenship No : {{ $userdata->citizenship_no }}
                                </div>

                                <div class="col-md-5 col-12 form-group">
                                   
                                        Citizenship Taken District : {{ $userdata->citizenship_district }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-12 form-group">
                                    
                                        Citizenship Date : {{ $userdata->citizenship_date }}
                                </div>
                                <div class="col-md-5 col-12 form-group">
                                   
                                        Date of birth : {{ $userdata->dob }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2 form-group">
                                    <button type="button" class="btn btn-primary edit" data-form="0">Edit</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Contact Information:</h2>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-12 form-group">
                                   
                                        Email : {{ $userdata->email }}
                                </div>

                                <div class="col-md-5 col-12 form-group">
                                   
                                        Mobile No : {{ $userdata->mobile_no }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-12 form-group">
                                    
                                        Correspondence Address : {{ $userdata->temporary_address }}
                                </div>

                                <div class="col-md-5 col-12 form-group">
                                        Permanent Address : {{ $userdata->permanent_address }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2 form-group">
                                    <button type="button" class="btn btn-primary edit" data-form="1">Edit</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">UG Qualification Detail:</h2>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-5 col-12 form-group">
                                   Degree Name : {{ $userdata->ug_degree }}
                                </div>

                                <div class="col-md-5 col-12 form-group">

                                   Degree From : {{ $userdata->degree_from }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-12 form-group">
                                   College Name : {{ $userdata->ug_college_name }}
                                </div>

                                <div class="col-md-5 col-12 form-group">
                                   Nepal Medical Council Reg Date : {{ $userdata->nmc_reg_date }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-12 form-group">
                                   Nepal Medical Council Reg No : {{ $userdata->nmc_reg_no }}
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-2 form-group">
                                    <button type="button" class="btn btn-primary edit" data-form="2">Edit</button>
                                </div>
                            </div>
                            @if(count($ugdocuments) >= 1)
                                    <table class="table table-bordered">
                                        <thead>
                                          <tr>
                                            <th>File name</th>
                                            <th width="50%"></th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($ugdocuments as $ug)
                                          <tr>
                                            <td>{{ $ug->document }}</td>
                                            <td><button type="button" class="btn btn-secondary view-file" data-file="{{$ug->document}}" data-type="{{$ug->type}}">View files</button></td>
                                          </tr>
                                          @endforeach
                                          
                                          
                                        </tbody>
                                      </table>
                                      @endif
                                
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">PG Qualification Detail:</h2>
                                </div>
                            </div>
                            <div class="row">
                                <input class="form-control" type="hidden" name="form_no" value="3" />

                                <div class="col-md-5 col-12 form-group">
                                   Degree Name : 
                                    {{-- <input disabled class="form-control" type="text" name="pg_degree" placeholder="Degree Name"
                                        value="{{ old('pg_degree', $userdata->pg_degree) }}" /> --}}
                                        {{  $userdata->pg_degree }}
                                </div>

                                <div class="col-md-5 col-12 form-group">

                                   Degree From: 
                                    {{-- <select disabled class="form-control" id="cars" name="pg_degree_from"
                                        value="{{ old('pg_degree_from', $userdata->pg_degree_from) }}">
                                        <option value="Nepali University">Nepali University</option>
                                        <option value="Foreign University">Foreign University</option>
                                    </select> --}}
                                    {{ $userdata->pg_degree_from }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-12 form-group">
                                   College Name : {{ $userdata->pg_college_name }}
                                    {{-- <input disabled class="form-control" type="text" name="pg_college_name"
                                        placeholder="Collage Name"
                                        value="{{ old('pg_college_name', $userdata->pg_college_name) }}" /> --}}
                                </div>

                                <div class="col-md-5 col-12 form-group">
                                   Mark Obtained : 
                                    {{-- <input disabled class="form-control" type="text" name="pg_marks_obtained"
                                        placeholder="Mark Obtained."
                                        value="{{ old('pg_marks_obtained', $userdata->pg_marks_obtained) }}" /> --}}
                                        {{ $userdata->pg_marks_obtained }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-12 form-group">
                                   Specialist Qualifying Exam Detail : 
                                    {{-- <input disabled class="form-control" type="text" name="sq_exam_detail"
                                        placeholder="Specialist Qualifying Exam Detail."
                                        value="{{ old('sq_exam_detail', $userdata->sq_exam_detail) }}" /> --}}
                                        {{ $userdata->sq_exam_detail }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2 form-group">
                                    <button type="button" class="btn btn-primary edit" data-form="3">Edit</button>
                                </div>
                                {{-- <div class="col-4 form-group doc-view" >
                                    <button type="button" class="btn btn-secondary view-file" data-form="3">View files</button>
                                </div> --}}
                            </div>
                            @if(count($pgdocuments) >= 1)
                            <table class="table table-bordered">
                                <thead>
                                  <tr>
                                    <th>File name</th>
                                    <th width="50%"></th>
                                  </tr>
                                </thead>
                                <tbody>
                                    @foreach($pgdocuments as $ug)
                                  <tr>
                                    <td>{{ $ug->document }}</td>
                                    <td><button type="button" class="btn btn-secondary view-file" data-file="{{$ug->document}}" data-type="{{$ug->type}}">View files</button></td>
                                  </tr>
                                  @endforeach
                                </tbody>
                              </table>
                              @endif
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Experience Detail:</h2>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-5 col-12 form-group">
                                   Affiliated : 
                                    
                                        {{ $userdata->affiliated  }}
                                </div>

                                <div class="col-md-5 col-12 form-group">

                                   Insitute/Hospital : 
                                   
                                        {{ $userdata->insitute_hospital }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-12 form-group">
                                   Position Head : 
                                   
                                        {{ $userdata->position_head }}
                                </div>

                                <div class="col-md-5 col-12 form-group">
                                   Period Of Experence : 
                                   
                                        {{ $userdata->experience }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2 form-group">
                                    <button type="button" class="btn btn-primary edit" data-form="4">Edit</button>
                                </div>
                               

                            </div>
                            @if(count($exdocuments) >= 1)
                            <table class="table table-bordered">
                                <thead>
                                  <tr>
                                    <th>File name</th>
                                    <th width="50%"></th>
                                  </tr>
                                </thead>
                                <tbody>
                                    @foreach($exdocuments as $ug)
                                  <tr>
                                    <td>{{ $ug->document }}</td>
                                    <td><button type="button" class="btn btn-secondary view-file" data-file="{{$ug->document}}" data-type="{{$ug->type}}">View file</button></td>
                                  </tr>
                                  @endforeach
                                </tbody>
                              </table>
                              @endif
                        @if($status == 0)

                            <div class="row proceed-left col-md-12">
                                <div class="col-md-12 form-group">
                                
                                <hr><button type="button" class="btn btn-success success btn-width proceed">Proceed for payment</button>
                                </div>
                                </div>
                        @endif
                        </div>
                        @if($status == 1)
                        <input type="button" name="next" class="submit action-button" value="Submit" />
                        @endif

                        {{-- <input type="button" name="next" class="submit action-button" value="Submit" />
                        <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> --}}
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>
    <script>
        var current = 0;
        $(document).ready(function() {
            $( ".edit" ).click(function() {
                var form_no = $(this).data('form');  
                gohome(form_no);

            });
            $(".proceed").click(function() {
                // if (validation()) {
                //     return true;
                // }
                submit();
            })
            $( ".action-button" ).click(function() {
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                })
                swalWithBootstrapButtons.fire({
                    title: 'Are you sure you want to submit application',
                    // text: "You want to upload bank Voucher!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Confirm',
                    cancelButtonText: 'Cancel',
                    reverseButtons: true
                }).then((result) => {
                    if (result.isConfirmed) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                console.log();
                $.ajax({
                    url: '/form-submit',
                    type: 'POST',
                    dataType: "json",
                  
                    success: function(data) {
                        Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Application successfully submitted',
                                showConfirmButton: false,
                                timer: 1500
                            })
                             .then((value) => {
                                
                                window.location.href= "/user-dashboard";
                                
                            });
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                       
                    }
                });
                    }
                    else
                    {
                        return 0;
                    }
                    });
               
            });
            $( ".view-file" ).click(function() {
                var file = $(this).data('file'); 
                var type = $(this).data('type');  
                file = '/document/'+file;

                if(type == "pdf")
                    {
                    $('.modal-body').empty().prepend($('<iframe>',{id:'theImg',src:file}));
                    $(".modal-header > h5").text("View file");
                    }  
                    else
                    {
                    $('.modal-body').empty().prepend($('<img>',{id:'theImg',src:file}));
                    $(".modal-header > h5").text("View file");
                    }
                    $('#myModel').modal('show');

                // $.ajaxSetup({
                //     headers: {
                //         'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                //     }
                // });
                // console.log();
                // $.ajax({
                //     url: '/preview-file',
                //     type: 'POST',
                //     dataType: "json",
                //     data: {
                //         'form_no': form_no
                //     },
                //     success: function(data) {
                //         let a = data.data;
                //         a.forEach(element => 
                //         {
                //             console.log(element)
                //             downloadURI(element.url)

                //         }
                //          );

                        
                //     },
                //     error: function(jqXHR, textStatus, errorThrown) {
                       
                //     }
                // });
               
            });
           
           
function downloadURI(uri, name='') 
{

    var link = document.createElement("a");
    // If you don't know the name or want to use
    // the webserver default set name = ''
    link.setAttribute('download', name);
    link.href = uri;
    document.body.appendChild(link);
    link.click();
    link.remove();
}
         
        
           function gohome(form_no)
            {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                let data = $('.form6').serialize();
                console.log();
                $.ajax({
                    url: '/edit-form-no',
                    type: 'POST',
                    dataType: "json",
                    data: {
                        'form_no': form_no
                    },
                    success: function(data) {
                        if(data.status == 200)
                        {
                            window.location.href= "/home/{!! $form !!}";
                        }
                        
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // Empty most of the time...
                    }
                });
               
            }
       

            function submit()
            {                              
                window.location.href= "/payment/{!! $form !!}";
                // $.ajaxSetup({
                //     headers: {
                //         'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                //     }
                // });
                // let data = $('.form6').serialize();
                // console.log();
                // $.ajax({
                //     url: 'store-fellowship-form',
                //     type: 'POST',
                //     dataType: "json",
                //     data: {
                //         data: data
                //     },
                //     success: function(data) {
                //         if (data.status === 200) {
                //             // nextOnSuccess(current, current_fs, next_fs)
                //             Swal.fire({
                //             title: 'BP Koirala FellowShip Application Successfully stored',
                //             confirmButtonText: `Proceed to payment`
                //             })
                //             .then((value) => {
                //             if(value.isConfirmed)
                //             {
                //             window.location.href= "/payment";
                //             }
                //             });
                //         }
                //     },
                //     error: function(jqXHR, textStatus, errorThrown) {
                //         // Empty most of the time...
                //     }
                // });
               
            }

        });

    
    </script>
@endpush

