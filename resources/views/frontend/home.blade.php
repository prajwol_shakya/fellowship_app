@extends('layouts.app')
@section('style')
    <style>
        #heading {
            text-transform: uppercase;
            color: #673AB7;
            font-weight: normal
        }

        .form-card {
            text-align: left;
            margin-left: 10%;

        }

        .form-group label {
            float: left;
        }

        #msform fieldset:not(:first-of-type) {
            display: none
        }


        #msform .action-button {
            width: 100px;
            background: #673AB7;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 5px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 30px 10px 5px;
            float: right;
        }

        #msform .action-button:hover,
        #msform .action-button:focus {
            background-color: #311B92
        }

        #msform .action-button-previous {
            width: 100px;
            background: #616161;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 5px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 5px 10px 0px;
            float: right
        }

        #msform .action-button-save {
            width: 100px;
            background: #28a745;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 5px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 5px 10px 0px;
            float: right
        }

        #msform .action-button-previous:hover,
        #msform .action-button-previous:focus {
            background-color: #000000
        }

        #msform .action-button-save:hover,
        #msform .action-button-save:focus {
            background-color: #0d3c18
        }

        .card {
            z-index: 0;
            border: none;
            position: relative
        }

        .fs-title {
            font-size: 25px;
            color: #673AB7;
            margin-bottom: 15px;
            font-weight: normal;
            text-align: left
        }

        .purple-text {
            color: #673AB7;
            font-weight: normal
        }

        .steps {
            color: gray;
            margin-bottom: 10px;
            font-weight: normal;
            text-align: right
        }

        /* .fieldlabels {
                                    color: gray;
                                    text-align: left
                                } */

        #progressbar {
            margin-bottom: 30px;
            overflow: hidden;
            color: lightgrey
        }

        #progressbar .active {
            color: #673AB7
        }

        #progressbar li {
            list-style-type: none;
            font-size: 15px;
            width: 18%;
            float: left;
            position: relative;
            font-weight: 400
        }

        #progressbar #account:before {
            font-family: FontAwesome;
            content: "\f13e"
        }

        #progressbar #personal:before {
            font-family: FontAwesome;
            content: "\f007"
        }

        #progressbar #payment:before {
            font-family: FontAwesome;
            content: "\f030"
        }

        #progressbar #confirm:before {
            font-family: FontAwesome;
            content: "\f00c"
        }

        #progressbar li:before {
            width: 50px;
            height: 50px;
            line-height: 45px;
            display: block;
            font-size: 20px;
            color: #ffffff;
            background: lightgray;
            border-radius: 50%;
            margin: 0 auto 10px auto;
            padding: 2px
        }

        #progressbar li:after {
            content: '';
            width: 100%;
            height: 2px;
            background: lightgray;
            position: absolute;
            left: 0;
            top: 25px;
            z-index: -1
        }

        #progressbar li.active:before,
        #progressbar li.active:after {
            background: #673AB7
        }

        .progress {
            height: 14px;
            box-shadow: inset 1px 2px 1px #21252924;
        }

        .progress-bar {
            background-color: #673AB7
        }

        .fit-image {
            width: 100%;
            object-fit: cover
        }

        label.error {
            color: red;
        }
        .dz-image > img{
            width: 75%;
            height: 111px;
        }

    </style>
@endsection
@section('content')
    <div class="container col-md-8">
        <div class="card card-custom row justify-content-center">
            <div class="text-center p-0 mt-3 mb-2">
                <div class="px-0 pt-4 pb-0 mt-3 mb-3">
                    <h1 id="heading">Fellowship Form</h1>
                    <form id="msform">
                        <!-- progressbar -->
                        <ul id="progressbar">
                            <li class="active"><strong>Personal</strong></li>
                            <li><strong>Contact</strong></li>
                            <li><strong>UG Qualification</strong></li>
                            <li><strong>PG Qualification</strong></li>
                            <li><strong>Experience</strong></li>
                            {{-- <li><strong>priview</strong></li> --}}


                        </ul>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <br> <!-- fieldsets -->
                        <fieldset class="form1" id="msform" name="first-form">
                            {{-- <form class="form1"> --}}
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-7">
                                            <h2 class="fs-title">Personal Information:</h2>
                                        </div>
                                        <div class="col-5">
                                            <p class="steps">Step 1 - 5</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <input class="form-control" type="hidden" name="form_no" value="0" />
                                        <div class="col-md-6 col-12 form-group">
                                            <label class="fieldlabels">Full Name: *</label>
                                            <input class="form-control" type="text" name="full_name" placeholder="Full Name"
                                                value="{{ old('full_name', $userdata->full_name) }}" />
                                        </div>

                                        <div class="col-md-6 col-12 form-group">
                                            <label class="fieldlabels">Father Name: *</label>
                                            <input class="form-control" type="text" name="father_name"
                                                placeholder="Father Name"
                                                value="{{ old('father_name', $userdata->father_name) }}" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-12 form-group">
                                            <label class="fieldlabels">Mother Name: *</label>
                                            <input class="form-control" type="text" name="mother_name"
                                                placeholder="Mother Name"
                                                value="{{ old('mother_name', $userdata->mother_name) }}" />
                                        </div>
                                        <div class="col-md-6 col-12 form-group">
                                            <label class="fieldlabels">Nationality: *</label>
                                            {{-- <input class="form-control" type="text"
                                                name="nationality" placeholder="Nationality"
                                                value="{{ old('nationality', $userdata->nationality) }}" />
                                            --}}
                                            <select class="form-control" name="nationality">
                                                <option value="null">Select nationality</option>
                                                @foreach ($countries as $country)
                                                    <option
                                                        {{ $country->country_name == old('nationality', $userdata->nationality) ? "selected='selected'" : '' }}
                                                        value="{{ $country->country_name }}">{{ $country->country_name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-12 form-group">
                                            <label class="fieldlabels">Citizenship No: *</label>
                                            <input class="form-control" type="text" name="citizenship_no"
                                                placeholder="Citizenship no"
                                                value="{{ old('citizenship_no', $userdata->citizenship_no) }}" />
                                        </div>

                                        <div class="col-md-6 col-12 form-group">
                                            <label class="fieldlabels">Citizenship Taken District.: *</label>
                                            <input class="form-control" type="text" name="citizenship_district"
                                                placeholder="Citizenship Taken District."
                                                value="{{ old('citizenship_district', $userdata->citizenship_district) }}" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-12 form-group">
                                            <label class="fieldlabels">Citizenship Date.: *</label>
                                            <input class="form-control" type="date" name="citizenship_date"
                                                placeholder="Citizenship Date."
                                                value="{{ old('citizenship_date', $userdata->citizenship_date) }}" />
                                        </div>
                                        <div class="col-md-6 col-12 form-group">
                                            <label class="fieldlabels">Date of birth.: *</label>
                                            <input class="form-control" type="date" name="dob" placeholder="Date of birth."
                                                value="{{ old('dob', $userdata->dob) }}" />
                                        </div>
                                    </div>
                                </div>

                                <input type="button" name="next" class="next action-button" value="Next" />
                                {{-- <input type="button" name="next"
                                    class="save action-button-save" value="Save" /> --}}

                                {{--
                            </form> --}}

                        </fieldset>
                        <fieldset class="form2" id="msform" name="second-form">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-7">
                                        <h2 class="fs-title">Contact Information:</h2>
                                    </div>
                                    <div class="col-5">
                                        <p class="steps">Step 2 - 5</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <input class="form-control" type="hidden" name="form_no" value="1" />
                                    <div class="col-md-6 col-12 form-group">
                                        <label class="fieldlabels">Email: *</label>
                                        <input class="form-control" type="text" name="email" placeholder="Email"
                                            value="{{ old('email', $userdata->email) }}" disabled />
                                    </div>

                                    <div class="col-md-6 col-12 form-group">
                                        <label class="fieldlabels">Mobile No: *</label>
                                        <input class="form-control" type="text" name="mobile_no" placeholder="Mobile No"
                                            value="{{ old('mobile_no', $userdata->mobile_no) }}" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12 form-group">
                                        <label class="fieldlabels">Correspondence Address: *</label>
                                        <input class="form-control" type="text" name="temporary_address"
                                            placeholder="Correspondence Address"
                                            value="{{ old('temporary_address', $userdata->temporary_address) }}" />
                                    </div>

                                    <div class="col-md-6 col-12 form-group">
                                        <label class="fieldlabels">Permanent Address: *</label>
                                        <input class="form-control" type="text" name="permanent_address"
                                            placeholder="Permanent Address"
                                            value="{{ old('permanent_address', $userdata->permanent_address) }}" />
                                    </div>
                                </div>
                            </div>
                            <input type="button" name="next" class="next action-button" value="Next" />
                            {{-- <input type="button" name="next"
                                class="save action-button-save" value="Save" /> --}}
                            <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                        </fieldset>
                        <fieldset class="form3">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-7">
                                        <h2 class="fs-title">UG Qualification Detail:</h2>
                                    </div>
                                    <div class="col-5">
                                        <p class="steps">Step 3 - 5</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <input class="form-control" type="hidden" name="form_no" value="2" />

                                    <div class="col-md-6 col-12 form-group">
                                        <label class="fieldlabels">Degree Name: *</label>
                                        <input class="form-control" type="text" name="ug_degree" placeholder="Degree Name"
                                            value="{{ old('ug_degree', $userdata->ug_degree) }}" />
                                    </div>

                                    <div class="col-md-6 col-12 form-group">

                                        <label class="fieldlabels">Degree From: *</label>
                                        <select class="form-control" id="cars" name="degree_from"
                                            value="{{ old('degree_from', $userdata->degree_from) }}">
                                            <option value="Nepali University">Nepali University</option>
                                            <option value="Foreign University">Foreign University</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12 form-group">
                                        <label class="fieldlabels">College Name: *</label>
                                        <input class="form-control" type="text" name="ug_college_name"
                                            placeholder="Collage Name"
                                            value="{{ old('ug_college_name', $userdata->ug_college_name) }}" />
                                    </div>

                                    <div class="col-md-6 col-12 form-group">
                                        <label class="fieldlabels">Nepal Medical Council Reg Date.: *</label>
                                        <input class="form-control" type="date" name="nmc_reg_date"
                                            placeholder="Nepal Medical Council Reg Date."
                                            value="{{ old('nmc_reg_date', $userdata->nmc_reg_date) }}" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12 form-group">
                                        <label class="fieldlabels">Nepal Medical Council Reg No.: *</label>
                                        <input class="form-control" type="text" name="nmc_reg_no"
                                            placeholder="Nepal Medical Council Reg No."
                                            value="{{ old('nmc_reg_no', $userdata->nmc_reg_no) }}" />
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-12 form-group">
                                        <label class="fieldlabels ugtab"> Tap or Drag files here to upload:</label>
                                        <div class="needsclick dropzone" id="ugdocument-dropzone">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="button" name="next" class="next action-button" value="Next" />
                            {{-- <input type="button" name="next"
                                class="save action-button-save" value="Save" /> --}}
                            <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                        </fieldset>
                        <fieldset class="form4" id="msform">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-7">
                                        <h2 class="fs-title">PG Qualification Detail:</h2>
                                    </div>
                                    <div class="col-5">
                                        <p class="steps">Step 4 - 5</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <input class="form-control" type="hidden" name="form_no" value="3" />

                                    <div class="col-md-6 col-12 form-group">
                                        <label class="fieldlabels">Degree Name: *</label>
                                        <input class="form-control" type="text" name="pg_degree" placeholder="Degree Name"
                                            value="{{ old('pg_degree', $userdata->pg_degree) }}" />
                                    </div>

                                    <div class="col-md-6 col-12 form-group">

                                        <label class="fieldlabels">Degree From: *</label>
                                        <select class="form-control" id="cars" name="pg_degree_from"
                                            value="{{ old('pg_degree_from', $userdata->pg_degree_from) }}">
                                            <option value="Nepali University">Nepali University</option>
                                            <option value="Foreign University">Foreign University</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12 form-group">
                                        <label class="fieldlabels">College Name: *</label>
                                        <input class="form-control" type="text" name="pg_college_name"
                                            placeholder="Collage Name"
                                            value="{{ old('pg_college_name', $userdata->pg_college_name) }}" />
                                    </div>

                                    <div class="col-md-6 col-12 form-group">
                                        <label class="fieldlabels">Mark Obtained.: *</label>
                                        <input class="form-control" type="text" name="pg_marks_obtained"
                                            placeholder="Mark Obtained."
                                            value="{{ old('pg_marks_obtained', $userdata->pg_marks_obtained) }}" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12 form-group">
                                        <label class="fieldlabels">Specialist Qualifying Exam Detail.: *</label>
                                        <input class="form-control" type="text" name="sq_exam_detail"
                                            placeholder="Specialist Qualifying Exam Detail."
                                            value="{{ old('sq_exam_detail', $userdata->sq_exam_detail) }}" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 form-group">
                                        <label class="fieldlabels pgtab">Tap or Drag files here to upload:</label>
                                        <div class="needsclick dropzone" id="pgdocument-dropzone">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="button" name="next" class="next action-button" value="Next" />
                            {{-- <input type="button" name="next"
                                class="save action-button-save" value="Save" /> --}}
                            <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                        </fieldset>
                        <fieldset class="form5" id="msform">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-7">
                                        <h2 class="fs-title">Experience Detail:</h2>
                                    </div>
                                    <div class="col-5">
                                        <p class="steps">Step 5 - 5</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <input class="form-control" type="hidden" name="form_no" value="4" />
                                    <div class="col-md-6 col-12 form-group">
                                        <label class="fieldlabels">Affiliated: *</label>
                                        <input class="form-control" type="text" name="affiliated" placeholder="affiliated"
                                            value="{{ old('affiliated', $userdata->affiliated) }}" />
                                    </div>

                                    <div class="col-md-6 col-12 form-group">

                                        <label class="fieldlabels">Insitute/Hospital: *</label>
                                        <input class="form-control" type="text" name="insitute_hospital"
                                            placeholder="Insitute/Hospital"
                                            value="{{ old('insitute_hospital', $userdata->insitute_hospital) }}" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12 form-group">
                                        <label class="fieldlabels">Position Head.: *</label>
                                        <input class="form-control" type="text" name="position_head"
                                            placeholder="Position Head."
                                            value="{{ old('position_head', $userdata->position_head) }}" />
                                    </div>

                                    <div class="col-md-6 col-12 form-group">
                                        <label class="fieldlabels">Period Of Experence.: *</label>
                                        <input class="form-control" type="text" name="experience"
                                            placeholder="Period Of Experience."
                                            value="{{ old('experience', $userdata->experience) }}" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 form-group">
                                        <label class="fieldlabels extab" >Tap or Drag files here to upload: </label>
                                        <div class="needsclick dropzone" id="exdocument-dropzone">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="button" name="next" class="next action-button" value="Next" />
                            <input type="button" name="previous" class="previous action-button-previous" value="Previous" />

                        </fieldset>
                       
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>
    <script>
        var uploadedDocumentMap = [];
        Dropzone.options.ugdocumentDropzone = {
            url: '{{ route('image') }}',
            maxFilesize: 20, // MB
            addRemoveLinks: true,
            params: {
                form_id: 2
            },
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dictDefaultMessage: 'Drop files here to upload (only pdf,jpeg,png,jpg formats)',

            acceptedFiles: "image/jpeg,image/png,application/pdf",
            accept: function(file, done) {
                console.log(file.type)
                switch (file.type) {
                case 'application/pdf':
                    this.emit("thumbnail", file, "/logo/pdf.png");            
                    break;
                case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                    this.emit("thumbnail", file, "/logo/word.gif"); 
                    break;
                case 'application/msword':
                    this.emit("thumbnail", file, "/logo/word.gif"); 
                    break;
                 case 'application/vnd.ms-excel':
                    this.emit("thumbnail", file, "/logo/excel.jpg"); 
                    break;
                case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                    this.emit("thumbnail", file, "/logo/excel.jpg"); 
                    break;
                case 'text/csv':
                    this.emit("thumbnail", file, "/logo/csv.svg"); 
                    break;
                    case 'image/jpeg':
                    this.emit("thumbnail", file); 
                    break;
                }
                file.previewTemplate.querySelector(".dz-image img").style.width="120px";

                done();
                },
    
            success: function(file, response) {
                uploadedDocumentMap[file.name] = response.name;
                console.log(uploadedDocumentMap)
            },
           
            removedfile: function(file) {
                console.log(file)
                var name = uploadedDocumentMap[file.name];
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    type: 'POST',
                    url: '{{ route('image.destroy') }}',
                    data: {
                        filename: name
                    },
                    success: function(data) {
                        if (data.status == 200) {
                            file.previewElement.remove();
                            var name = ''
                            if (typeof file.file_name !== 'undefined') {
                                name = file.file_name
                            } else {
                                name = uploadedDocumentMap[file.name]
                            }
                            $('form').find('input[name="document[]"][value="' + name + '"]').remove()
                            console.log("File has been successfully removed!!");
                        }
                    },
                    error: function(e) {
                        console.log(e)
                    }
                });

            },
            init: function() {
                let myDropzone = this;
                $.ajax({
                    type: 'get',
                    data: {
                        form_id: 2
                    },
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    url: '{{ route('image.get') }}',
                    success: function(data) {
                        $.each(data.data, function(key, value) {
                            let mockFile = {
                                name: value.name,
                                size: 1024
                            };
                            uploadedDocumentMap[value.name] = value.name;
                            myDropzone.emit("addedfile", mockFile);
                            myDropzone.options.thumbnail.call(myDropzone, mockFile, value.url);
                            // Make sure that there is no progress bar, etc...
                            myDropzone.emit("complete", mockFile);
                            var ext = value.name.split('.').pop();
                            if (ext == "pdf") {
                                myDropzone.emit("thumbnail", mockFile, "/logo/pdf.png");
                                // $(data.previewElement).find(".dz-image img").attr("src", "/public/logo/pdf.png");
                            } else if (ext.indexOf("doc") != -1) {
                                myDropzone.emit("thumbnail", mockFile, "/logo/word.gif");
                            } else if (ext.indexOf("xls") != -1) {
                                myDropzone.emit("thumbnail", mockFile, "/logo/excel.jpg");
                            } else if (ext.indexOf("xlsx") != -1) {
                                myDropzone.emit("thumbnail", mockFile, "/logo/excel.jpg");

                            }
                        });
                    },
                    error: function(xhr, durum, hata) {
                        alert("Hata: " + hata);
                    }
                });

            }
        }
        var pguploadedDocumentMap = [];
        Dropzone.options.pgdocumentDropzone = {
            url: '{{ route('image') }}',
            maxFilesize: 20, // MB
            addRemoveLinks: true,
            params: {
                form_id: 3
            },
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dictDefaultMessage: 'Drop files here to upload (only pdf,jpeg,png,jpg formats)',
            acceptedFiles: "image/jpeg,image/png,application/pdf",
            success: function(file, response) {
                // console.log(response.name)
                // $('form').append('<input type="hidden" name="document[]" value="' + response.name + '">')
                // pguploadedDocumentMap[file.name] = response.name;
                // file.name = response.name
                pguploadedDocumentMap[file.name] = response.name;

            },
            accept: function(file, done) {
                console.log(file.type)
                switch (file.type) {
                case 'application/pdf':
                    this.emit("thumbnail", file, "/logo/pdf.png");            
                    break;
                case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                    this.emit("thumbnail", file, "/logo/word.gif"); 
                    break;
                case 'application/msword':
                    this.emit("thumbnail", file, "/logo/word.gif"); 
                    break;
                 case 'application/vnd.ms-excel':
                    this.emit("thumbnail", file, "/logo/excel.jpg"); 
                    break;
                case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                    this.emit("thumbnail", file, "/logo/excel.jpg"); 
                    break;
                case 'text/csv':
                    this.emit("thumbnail", file, "/logo/csv.svg"); 
                    break;
                }
                file.previewTemplate.querySelector(".dz-image img").style.width="120px";

                done();
                },
            removedfile: function(file) {
                console.log(file)
                var name =  pguploadedDocumentMap[file.name];
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    type: 'POST',
                    url: '{{ route('image.destroy') }}',
                    data: {
                        filename: name
                    },
                    success: function(data) {
                        if (data.status == 200) {
                            file.previewElement.remove();
                            var name = ''
                            if (typeof file.file_name !== 'undefined') {
                                name = file.file_name
                            } else {
                                name = uploadedDocumentMap[file.name]
                            }
                            $('form').find('input[name="document[]"][value="' + name + '"]').remove()
                            console.log("File has been successfully removed!!");
                        }
                    },
                    error: function(e) {
                        console.log(e)
                    }
                });

            },
            init: function() {

                let myDropzone = this;
                $.ajax({
                    type: 'get',
                    data: {
                        form_id: 3
                    },
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    url: '{{ route('image.get') }}',
                    success: function(data) {
                        $.each(data.data, function(key, value) {
                            let mockFile = {
                                name: value.name,
                                size: 1024
                            };
                            pguploadedDocumentMap[value.name] = value.name;

                            myDropzone.emit("addedfile", mockFile);
                            myDropzone.options.thumbnail.call(myDropzone, mockFile, value.url);
                            // Make sure that there is no progress bar, etc...
                            myDropzone.emit("complete", mockFile);
                            
                            var ext = value.name.split('.').pop();
                            if (ext == "pdf") {
                                myDropzone.emit("thumbnail", mockFile, "/logo/pdf.png");
                                // $(data.previewElement).find(".dz-image img").attr("src", "/public/logo/pdf.png");
                            } else if (ext.indexOf("doc") != -1) {
                                myDropzone.emit("thumbnail", mockFile, "/logo/word.gif");
                            } else if (ext.indexOf("xls") != -1) {
                                myDropzone.emit("thumbnail", mockFile, "/logo/excel.jpg");
                            } else if (ext.indexOf("xlsx") != -1) {
                                myDropzone.emit("thumbnail", mockFile, "/logo/excel.jpg");

                            }
                        });
                    },
                    error: function(xhr, durum, hata) {
                        alert("Hata: " + hata);
                    }
                });

            }
        }
        var exuploadedDocumentMap = [];
        Dropzone.options.exdocumentDropzone = {
            url: '{{ route('image') }}',
            maxFilesize: 10, // MB
            addRemoveLinks: true,
            clickable: true,
            params: {
                form_id: 4
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dictDefaultMessage: 'Drop files here to upload (only pdf,jpeg,png,jpg formats)',
            acceptedFiles: "image/jpeg,image/png,application/pdf",
            accept: function(file, done) {
                console.log(file.type)
                switch (file.type) {
                case 'application/pdf':
                    this.emit("thumbnail", file, "/logo/pdf.png");            
                    break;
                case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                    this.emit("thumbnail", file, "/logo/word.gif"); 
                    break;
                case 'application/msword':
                    this.emit("thumbnail", file, "/logo/word.gif"); 
                    break;
                 case 'application/vnd.ms-excel':
                    this.emit("thumbnail", file, "/logo/excel.jpg"); 
                    break;
                case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                    this.emit("thumbnail", file, "/logo/excel.jpg"); 
                    break;
                case 'text/csv':
                    this.emit("thumbnail", file, "/logo/csv.svg"); 
                    break;
                }
                file.previewTemplate.querySelector(".dz-image img").style.width="120px";

                done();
                },
            success: function(file, response) {
                console.log(response.name)
                exuploadedDocumentMap[file.name] = response.name;
            },
            removedfile: function(file) {
                console.log(file)
                var name = exuploadedDocumentMap[file.name];
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    type: 'POST',
                    url: '{{ route('image.destroy') }}',
                    data: {
                        filename: name
                    },
                    success: function(data) {
                        if (data.status == 200) {
                            file.previewElement.remove();
                            var name = ''
                            if (typeof file.file_name !== 'undefined') {
                                name = file.file_name
                            } else {
                                name = uploadedDocumentMap[file.name]
                            }
                            $('form').find('input[name="document[]"][value="' + name + '"]').remove()
                            console.log("File has been successfully removed!!");
                        }
                    },
                    error: function(e) {
                        console.log(e)
                    }
                });

            },
            init: function() {

                let myDropzone = this;
                $.ajax({
                    type: 'get',
                    data: {
                        form_id: 4
                    },
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    url: '{{ route('image.get') }}',
                    success: function(data) {
                        $.each(data.data, function(key, value) {
                            let mockFile = {
                                name: value.name,
                                size: 1024
                            };
                            exuploadedDocumentMap[value.name]=value.name;
                            myDropzone.emit("addedfile", mockFile);
                            myDropzone.options.thumbnail.call(myDropzone, mockFile, value.url);
                            // Make sure that there is no progress bar, etc...
                            myDropzone.emit("complete", mockFile);
                            var ext = value.name.split('.').pop();
                            if (ext == "pdf") {
                                myDropzone.emit("thumbnail", mockFile, "/logo/pdf.png");
                                // $(data.previewElement).find(".dz-image img").attr("src", "/public/logo/pdf.png");
                            } else if (ext.indexOf("doc") != -1) {
                                myDropzone.emit("thumbnail", mockFile, "/logo/word.gif");
                            } else if (ext.indexOf("xls") != -1) {
                                myDropzone.emit("thumbnail", mockFile, "/logo/excel.jpg");
                            } else if (ext.indexOf("xlsx") != -1) {
                                myDropzone.emit("thumbnail", mockFile, "/logo/excel.jpg");

                            }
                        });
                    },
                    error: function(xhr, durum, hata) {
                        alert("Hata: " + hata);
                    }
                });

            }
        }

    </script>
    <script>
        var current = 0;
        $(document).ready(function() {
            var form_no = {{ $userdata->form_no == null ? 0 : $userdata->form_no}};
            // form_no = form_no  == 1 ? 1 : form_no; 
            resetlink(form_no);
            resetfieldset(form_no);
            var current_fs, next_fs, previous_fs; //fieldsets
            var opacity;
            current = form_no + 1;
            var steps = 5;
            setProgressBar(current);
            $(".next").click(function() {
                current_fs = $(this).parent();
                next_fs = $(this).parent().next();
                console.log()
                if (validation()) {
                    return true;
                }
                StoreUserInfo(current, current_fs, next_fs);
                if (current <= 4) {
                    nextOnSuccess(current, current_fs, next_fs)
                    current += 1;

                }
            });

            $(".submit").click(function() {
                if (validation()) {
                    return true;
                }
                submit();
            })
            $(".save").click(function() {
                current_fs = $(this).parent();
                next_fs = $(this).parent().next();
                if (validation()) {
                    return true;
                }
                StoreUserInfo(current, current_fs, next_fs);
                if (current <= 5) {
                    current += 1;

                }
            })
            $(".previous").click(function() {

                current_fs = $(this).parent();
                previous_fs = $(this).parent().prev();

                //Remove class active
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                //show the previous fieldset
                previous_fs.show();

                //hide the current fieldset with style
                current_fs.animate({
                    opacity: 0
                }, {
                    step: function(now) {
                        // for making fielset appear animation
                        opacity = 1 - now;

                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        previous_fs.css({
                            'opacity': opacity
                        });
                    },
                    duration: 500
                });
                let x = --current;
                setProgressBar(x);

                resetlink(--x);

            });

            $(".extab").click(function() {
               myDropzone= $('#exdocument-dropzone').get(0).dropzone;

                myDropzone.hiddenFileInput.click()

            })
            $(".pgtab").click(function() {
               myDropzone= $('#pgdocument-dropzone').get(0).dropzone;

                myDropzone.hiddenFileInput.click()

            })
            $(".ugtab").click(function() {
               myDropzone= $('#ugdocument-dropzone').get(0).dropzone;

                myDropzone.hiddenFileInput.click()

            })
            function validation() {
                var status = false;
                // $('fieldset').each(function(){
                console.log($(this))
                $('#msform').validate({
                    focusCleanup: true,
                    rules: {
                        full_name: {
                            required: true,
                        },
                        father_name: {
                            required: true,
                        },
                        mother_name: {
                            required: true,
                        },
                        nationality: {
                            required: true,
                        },
                        citizenship_no: {
                            required: true,
                        },
                        citizenship_district: {
                            required: true,
                        },
                        citizenship_date: {
                            required: true,
                        },
                        dob: {
                            required: true,
                        },
                        mobile_no: {
                            required: true,
                        },
                        temporary_address: {
                            required: true,
                        },
                        permanent_address: {
                            required: true,
                        },
                        degree_from: {
                            required: true,
                        },
                        nmc_reg_date: {
                            required: true,
                        },
                        ug_degree: {
                            required: true,
                        },
                        ug_college_name: {
                            required: true,
                        },
                        nmc_reg_no: {
                            required: true,
                        },
                        pg_degree_from: {
                            required: true,
                        },
                        pg_degree: {
                            required: true,
                        },
                        pg_college_name: {
                            required: true,
                        },
                        pg_marks_obtained: {
                            required: true,
                        },
                        sq_exam_detail: {
                            required: true,
                        },
                        affiliated: {
                            required: true,
                        },
                        insitute_hospital: {
                            required: true,
                        },
                        experience: {
                            required: true,
                        },
                        position_head: {
                            required: true,
                        }

                    },
                    messages: {
                        nationality: "Enter your  nationality",
                        mother_name: "Enter your mother name",
                        father_name: "Enter your father name",
                        full_name: "Enter your full name",
                        citizenship_no: "Enter your citizenship no",
                        citizenship_district: "Enter your citizenship district",
                        dob: "Enter your date of birth",
                        citizenship_date: "Enter your citizenship taken date",
                        mobile_no: "Enter your  mobile no",
                        temporary_address: "Enter your correspondence address",
                        permanent_address: "Enter your permanent address",

                        degree_from: "Select undergraduate university",
                        nmc_reg_date: "Enter your nepal medical council reg taken date",
                        ug_degree: "Enter your undergraduate degree name",
                        ug_college_name: "Enter your undergraduate college name",
                        nmc_reg_no: "Enter your medical council reg no ",
                        pg_degree: "Enter your postgraduate degree name",
                        pg_degree_from: "Enter your postgraduate degree from",
                        pg_college_name: "Enter your postgraduate college name",
                        pg_marks_obtained: "Enter your postgraduate mark obtained",
                        sq_exam_detail: "Enter specialist qualifying exam detail",
                        affiliated: "Enter your affiliation",
                        insitute_hospital: "Enter insitute/hospital",
                        experience: "Enter your experience",
                        position_head: "Enter your position",

                    }
                });
                if (!$('#msform').valid()) {
                    status = true;
                }


                // });
                return status;

            }

            function nextOnSuccess(current, current_fs, next_fs) {

                //Add Class Active
                // $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                resetlink(current);

                //show the next fieldset
                next_fs.show();
                //hide the current fieldset with style
                current_fs.animate({
                    opacity: 0
                }, {
                    step: function(now) {
                        // for making fielset appear animation
                        opacity = 1 - now;

                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        next_fs.css({
                            'opacity': opacity
                        });
                    },
                    duration: 500
                });
                setProgressBar(current);


            }

            function setProgressBar(curStep) {
                var percent = parseFloat(100 / steps) * curStep;
                console.log(percent)
                percent = percent.toFixed();
                $(".progress-bar")
                    .css("width", percent + "%")
            }


            function StoreUserInfo(current, current_fs, next_fs) {

                switch (current) {
                    case 1:
                        return personalstore(current, current_fs, next_fs);
                        break;
                    case 2:
                        return Contact(current, current_fs, next_fs);
                        break;
                    case 3:
                        return ugQulification(current, current_fs, next_fs);
                        break;
                    case 4:
                        return pgQulification(current, current_fs, next_fs);
                        break;
                    case 5:
                        return experience(current, current_fs, next_fs);
                        break;

                }
            }

            function personalstore(current, current_fs, next_fs) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                let data = $('.form1').serialize();
                console.log();
                $.ajax({
                    url: '/store-fellowship-form',
                    type: 'POST',
                    dataType: "json",
                    data: {
                        data: data
                    },
                    success: function(data) {
                        if (data.status === 200) {
                            nextOnSuccess(current, current_fs, next_fs)

                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Personal Info successfully stored',
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // Empty most of the time...
                    }
                });
            }

            function Contact(current, current_fs, next_fs) {
                $('#msform').validate({
                    focusCleanup: true,
                    rules: {
                        mobile_no: {
                            required: true,
                        },
                        temporary_address: {
                            required: true,
                        },
                        permanent_address: {
                            required: true,
                        },



                    },
                    messages: {
                        mobile_no: "Please enter your  mobile no",
                        temporary_address: "Please enter your correspondence address",
                        permanent_address: "Please enter your permanent address",

                    }
                });

                if (!$('#msform').valid()) {
                    // $('#msform').resetForm();
                    return true;
                }
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                let data = $('.form2').serialize();
                console.log();
                $.ajax({
                    url: '/store-fellowship-form',
                    type: 'POST',
                    dataType: "json",
                    data: {
                        data: data
                    },
                    success: function(data) {
                        if (data.status === 200) {
                            nextOnSuccess(current, current_fs, next_fs)
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Contact Info successfully stored',
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // Empty most of the time...
                    }
                });
            }

            function ugQulification(current, current_fs, next_fs) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                let data = $('.form3').serialize();
                console.log();
                $.ajax({
                    url: '/store-fellowship-form',
                    type: 'POST',
                    dataType: "json",
                    data: {
                        data: data
                    },
                    success: function(data) {
                        if (data.status === 200) {
                            nextOnSuccess(current, current_fs, next_fs)
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'UG Qualification successfully stored',
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // Empty most of the time...
                    }
                });
            }

            function pgQulification(current, current_fs, next_fs) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                let data = $('.form4').serialize();
                console.log();
                $.ajax({
                    url: '/store-fellowship-form',
                    type: 'POST',
                    dataType: "json",
                    data: {
                        data: data
                    },
                    success: function(data) {
                        if (data.status === 200) {
                            nextOnSuccess(current, current_fs, next_fs)
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'PG Qualification successfully stored',
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // Empty most of the time...
                    }
                });
            }

            function experience(current, current_fs, next_fs) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                let data = $('.form5').serialize();
                let package ={'data': data,'slug':'{!! $form !!}'};
                $.ajax({
                    url: '/store-fellowship-form-payment',
                    type: 'POST',
                    dataType: "json",
                    data:package,
                    success: function(data) {
                        if (data.status === 200) {
                            setProgressBar(current);
                            // nextOnSuccess(current, current_fs, next_fs)
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Experience successfully stored',
                                showConfirmButton: false,
                                timer: 1500
                            })
                             .then((value) => {
                                
                                window.location.href= "/preview/{!! $form !!}";
                                
                            });
                            // Swal.fire({
                            //     title: 'BP Koirala FellowShip Application Successfully stored',
                            //     confirmButtonText: `Proceed to payment`
                            // })
                            // .then((value) => {
                            //     if(value.isConfirmed)
                            //     {
                            //         window.location.href= "/payment";
                            //     }
                            // });

                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // Empty most of the time...
                    }
                });
            }
        
            function submit()
            {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                let data = $('.form6').serialize();
                console.log();
                $.ajax({
                    url: 'store-fellowship-form',
                    type: 'POST',
                    dataType: "json",
                    data: {
                        data: data
                    },
                    success: function(data) {
                        if (data.status === 200) {
                            // nextOnSuccess(current, current_fs, next_fs)
                            Swal.fire({
                            title: 'BP Koirala FellowShip Application Successfully stored',
                            confirmButtonText: `Proceed to payment`
                            })
                            .then((value) => {
                            if(value.isConfirmed)
                            {
                            window.location.href= "/payment";
                            }
                            });
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // Empty most of the time...
                    }
                });
               
            }

        });

        function resetlink(x) {
            $('#progressbar >li').each(function(i) {
                if (x == i) {
                    $(this).addClass('active');
                } else {
                    $(this).removeClass('active');
                }
            });
        }

        function resetfieldset(x) {
            $('fieldset').each(function(i) {
                if (x == i) {
                    $(this).css({
                        'display': 'block',
                        'opacity': 1
                    });
                } else {
                    $(this).css({
                        'display': 'none',
                        'position': 'relative',
                        'opacity': 0
                    });
                }
            });
        }

    </script>
@endpush
