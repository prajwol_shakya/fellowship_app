@extends('layouts.app')
@section('content')

<div class="content main-content">
    <div class="container">
        <div class="inner-content">
            <h1>Sorry!! Your transaction has been failed.</h1>
            <a href="{{route('user.form')}}" class="btn btn-info">Go back home</a>
        </div>
    </div>
</div>

@endsection
