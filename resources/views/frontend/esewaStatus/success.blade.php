@extends('layouts.app')
@section('content')

<div class="content main-content">
    <div class="container">
        <div class="inner-content">
            <h1> Congratulation Your transaction has been successful.</h1>
            <a href="{{route('user.dashboard')}}" class="btn btn-info">Go back Dashboard</a>
        </div>
    </div>
</div>

@endsection
