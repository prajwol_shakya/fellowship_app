@extends('layouts.app')
@push('style')
    <style>
        .fellowship-programs .card-title {
            margin: 30px 30px -15px 30px;
        }

        .card.col-md-10.col-12.fellowship-programs {
            color: #333;
            margin-bottom: 30px;

        }

    </style>
@endpush
@section('content')

<div class="container-fluid info-box">
    <div class="row">
        <div class="content-box col-md-9">
            <h1>Choose A FellowShip Program</h1>
            <p>&nbsp;</p>
            @foreach ($forms as $form)
                <div class="card col-md-10 col-12 fellowship-programs">
                    <div class="card-content">
                        <div class="card-title">
                            <h2>{{ $form->name }}</h2>

                            <hr>
                            <h6>Application Deadline: {{ $form->to }}</h6>
                        </div>
                        <div class="card-body">
                            <p>
                                {{ $form->description }}
                            </p>
                            <h6>Price: {{ $form->amount }}</h6>
                            <p>&nbsp;</p>
                            <button class="btn btn-primary check" data-slug="{{ $form->slug }}">Apply</button>
                        </div>
                    </div>

                </div>
            @endforeach

        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    $(document).ready(function() {
        $(".check").click(function() {
            var slug = $(this).data('slug');
            setcookies(slug);

        });

        function setcookies(slug) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            console.log();
            $.ajax({
                url: '/set-cookies',
                type: 'POST',
                dataType: "json",
                data: {
                    'slug': slug
                },
                success: function(data) {
                    if (data.status == 200) {
                        window.location.href = "/login";
                    } 
                    else
                    {
                        Swal.fire({
                                        icon: 'error',
                                        title: data.message,
                                        confirmButtonText: `ok`,

                                    });
                    }

                },
                error: function(jqXHR, textStatus, errorThrown) {

                }
            });
        }
    });

</script>
@endpush
