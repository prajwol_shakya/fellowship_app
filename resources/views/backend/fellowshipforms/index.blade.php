@extends('layouts.app')

@section('content')

    <div class="modal fade bd-example-modal-lg" id="myModel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> create fellowship form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="myform">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name='name' class="form-control" id="name">
                        </div>
                        <div class="form-group">
                            <label for="Description">Description</label>
                            <textarea name="description" class="form-control" id="Description" rows="4" cols="50">
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label for="Amount">Amount</label>
                            <input type="number" inputmode="decimal" name="amount" class="form-control" id="Amount">
                        </div>
                        <div class="row">
                            <div class="col-sm">
                                <div class="form-group">
                                    <label for="Amount">From Date</label>
                                    <input class="datepicker form-control"  type="text"  name="from" id="fromdate" data-date-format="yyyy-mm-dd">
                                </div>
                            </div>
                            <div class="col-sm">
                                <div class="form-group">
                                    <label for="Amount">To Date</label>
                                    <input class="datepicker form-control"  type="text"  name="to" id="todate" data-date-format="yyyy-mm-dd">
                                </div>
                            </div>
                            
                          </div>
                          <div class="col-sm">
                            <div class="form-group">
                                <label for="Amount">Email format</label>
                                <textarea id="email_format1" class="email_format"></textarea>
                            </div>
                          </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary store">Save changes</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg" id="editModel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> update fellowship form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="editform">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name='name' class="form-control" id="name">
                        </div>
                        <div class="form-group">
                            <label for="Description">Description</label>
                            <textarea name="description" class="form-control" id="description" rows="4" cols="50">
                            </textarea>

                            {{-- <input type="text" name="description" class="form-control" id="Description"> --}}
                        </div>
                        <div class="form-group">
                            <label for="Amount">Amount</label>
                            <input type="number" inputmode="decimal" name="amount" class="form-control" id="Amount">
                        </div>
                        <div class="row">
                            <div class="col-sm">
                                <div class="form-group">
                                    <label for="Amount">From Date</label>
                                    <input class="datepicker-from form-control"  type="text"  name="from" id="fromdate" data-date-format="yyyy-mm-dd">
                                </div>
                            </div>
                            <div class="col-sm">
                                <div class="form-group">
                                    <label for="Amount">To Date</label>
                                    <input class="datepicker-to form-control"  type="text"  name="to" id="todate" data-date-format="yyyy-mm-dd">
                                </div>
                            </div>
                            
                          </div>
                          <div class="col-sm">
                            <div class="form-group">
                                <label for="Amount">Email format</label>
                                <textarea id="email_format2" class="email_format" ></textarea>
                            </div>
                          </div>
                    
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary edit">Update</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
    <div class="container-fluid">
        <button class="btn btn-primary create">create fellowship</button>

        <table class="table table-bordered" id="users-table">
            <thead>
                <tr>
                    <th>Fellowship Form</th>
                    {{-- <th>Description</th> --}}
                    <th>Total Applicants</th>
                    {{-- <th>Expired date</th> --}}
                    <th>Status</th>
                    <th>Action</th>

                </tr>
            </thead>
        </table>
    </div>
    </div>
@endsection
@push('style')
    <style>
        #theImg {
            height: 450px;
            width: 750px;

        }

        label.error {
            color: red;
        }
        .create
        {
            margin-bottom: 1%;
            float: right;
        }
    </style>
@endpush
@push('script')
<script src="https://cdn.tiny.cloud/1/bgrgbza8p6ijqah7i3beahxvrp4h89v0zt9on3w48bxntj19/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>

    <script>
     
tinymce.init({
  selector: '.email_format',
  height: 300,
  menubar: false,
  setup: function (editor) {
    editor.on('init', function(args) {
        editor = args.target;
        editor.on('NodeChange', function(e) {
        tinyMCE.DOM.setAttribs(e.element, {'width': 628, 'height': 376});
        });
    });
  },
//   image_dimensions: false,
        //  image_class_list: [
        //     {title: 'Responsive', value: 'img-responsive'}
        // ],
  plugins: [
    'advlist autolink lists link image charmap print preview anchor textcolor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code help wordcount'
  ],
  mobile: { 
    theme: 'mobile' 
  },
  images_upload_handler: function (blobInfo, success, failure) {
           var xhr, formData;
           xhr = new XMLHttpRequest();
           xhr.withCredentials = false;
           xhr.open('POST', '/upload-image');
           var token = '{{ csrf_token() }}';
           xhr.setRequestHeader("X-CSRF-Token", token);
           xhr.onload = function() {
               var json;
               if (xhr.status != 200) {
                   failure('HTTP Error: ' + xhr.status);
                   return;
               }
               json = JSON.parse(xhr.responseText);
               if (!json || typeof json.location != 'string') {
                   failure('Invalid JSON: ' + xhr.responseText);
                   return;
               }
               success(json.location);
           };
           formData = new FormData();
           formData.append('file', blobInfo.blob(), blobInfo.filename());
           xhr.send(formData);
       },

  toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tiny.cloud/css/codepen.min.css'
  ],
});

        $(function() {
            $('#fromdate').datepicker();
            $('#todate').datepicker();


            let form_id ;
                    $('.create').click(function() {
                    $('input[name="name"]').val('');
                    $('#description').val('');
                    $('#mytextarea').val('');

                    $('input[name="amount"]').val('');
                    $('#fromdate').val('');
                    $('#todate').val('');
                    $('#myModel').modal('show');

                    });
                    let table = loadtable();

                    function loadtable() {
                        $("#users-table").DataTable().destroy();
                        let table = $('#users-table').DataTable({
                            processing: true,
                            serverSide: true,
                            ajax: '{!!  route('form.list') !!}',

                            columns: [{
                                    data: 'name',
                                    name: 'name'
                                },
                                // {
                                //     data: 'description',
                                //     name: 'description'
                                // },
                                {
                                    data: 'applicant',
                                    name: 'applicant'
                                },
                                // {
                                //     data: 'to',
                                //     name: 'to'
                                // },
                                {
                                    data: 'status',
                                    name: 'status'
                                },
                                {
                                    data: 'action',
                                    name: 'action'
                                }
                            ],
                           

                        });
                        return table;
                    }
                    table.on('draw.dt', function(data) {
                        console.log(data)
                        $('.actionBtnTable').change(function() {
                            $('.actionBtnTable').not($(this)).prop('selectedIndex', 0);

                            if (this.value == 1) {
                                $('#mytextarea').val('');
                                $('input[name="name"]').val('');
                                $('input[name="description"]').val('');
                                $('input[name="amount"]').val(parseFloat(0));
                                var name = $(this).find(':selected').data('name');
                                var email_format = $(this).find(':selected').data('email_format');
                                var description = $(this).find(':selected').data('description');

                                var amount = $(this).find(':selected').data('amount');
                                var id = $(this).find(':selected').data('id');
                                var from = $(this).find(':selected').data('from');
                                var to = $(this).find(':selected').data('to');
                                form_id = id;
                            //    from = new Date(from);
                            //    to = new Date(to);
                                console.log('email format',email_format)
                                $('input[name="name"]').val(name);
                                $('#description').val(description);
                                // my.get("#mytextarea").setContent(email_format);
                                // my.activeEditor.setContent('custom');
                                tinymce.get('email_format2').setContent(email_format);

                                // $('#mytextarea').val(email_format);

                                $('input[name="amount"]').val(parseFloat(amount));
                                $('.datepicker-from').datepicker('update', '');
                                $('.datepicker-to').datepicker('update', '');
                                $('.datepicker-from').datepicker('update', from);
                                $('.datepicker-to').datepicker('update', to);
                                $('#editModel').modal('show');
                              
                               
                            } else if (this.value == 2) {
                                var id = $(this).find(':selected').data('id');
                                Swal.fire({
                                    title: 'Are you sure you want to Delete form?',
                                    text: "You won't be able to revert this!",
                                    icon: 'warning',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Yes, Delete it!'
                                    }).then((result) => {
                                    if (result.isConfirmed) {
                                            formdelete(id);
                                    }
                                    });

                            }
                            else  {
                                var id = $(this).find(':selected').data('id');
                                window.location.href= "/user-applied-form-list/"+id;


                            }
                        });
                    });

                    function formdelete(id)
                    {
                        $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                                    }
                                });
                                $.ajax({
                                    url: 'form-delete',
                                    type: 'POST',
                                    dataType: "json",
                                    data: {
                                        'id': id
                                    },
                                    success: function(data) {
                                        if (data.status === 200) {
                                            $('#myModel').modal('hide');
                                            Toast.fire({
                                                icon: 'success',
                                                title: data.message
                                            })
                                            loadtable();
                                        }
                                    },
                                    error: function(jqXHR, textStatus, errorThrown) {
                                        // Empty most of the time...
                                    }
                                });
                    }

                    function validation() {
                        var status = false;
                        // $('fieldset').each(function(){
                        console.log($(this))
                        $('#myform').validate({
                            focusCleanup: true,
                            rules: {
                                name: {
                                    required: true,
                                },
                                description: {
                                    required: true,
                                },
                                amount: {
                                    required: true,
                                },
                                from: {
                                    required: true,
                                },
                                to: {
                                    required: true,
                                },
                            },
                            messages: {
                                name: "Enter Title",
                                description: "Enter description",
                                amount: "Enter amount",
                                from: "Enter start of date of form submission", 
                                to: "Enter end date of submission",

                            }
                        });
                        if (!$('#myform').valid()) {
                            status = true;
                        }


                        // });
                        return status;

                    }
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })


                    $('.store').click(function() {
                                if (validation()) {
                                    return true;
                                }
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                                    }
                                });
                                let data = $('#myform').serialize();
                                console.log(data);
                                $.ajax({
                                    url: 'create-fellowship',
                                    type: 'POST',
                                    dataType: "json",
                                    data: {
                                        'data': data
                                    },
                                    success: function(data) {
                                        if (data.status === 200) {
                                            $('#myModel').modal('hide');

                                            Toast.fire({
                                                icon: 'success',
                                                title: data.message
                                            })
                                            loadtable();
                                        }
                                    },
                                    error: function(jqXHR, textStatus, errorThrown) {
                                        // Empty most of the time...
                                    }
                                });
                    });


                    $('.edit').click(function() {
                                if (validation()) {
                                    return true;
                                }
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                                    }
                                });
                                let data = $('#editform').serialize();
                                console.log(data);
                                $.ajax({
                                    url: 'update-fellowship/'+form_id,
                                    type: 'POST',
                                    dataType: "json",
                                    data: {
                                        'data': data,
                                        'email_format':tinyMCE.activeEditor.getContent()
                                    },
                                    success: function(data) {
                                        if (data.status === 200) {
                                            $('#editModel').modal('hide');
                                            Toast.fire({
                                                icon: 'success',
                                                title: data.message
                                            })
                                                loadtable();
                                        
                                        }
                                    },
                                    error: function(jqXHR, textStatus, errorThrown) {
                                        // Empty most of the time...
                                    }
                                });
                    });
    });

    </script>

@endpush
