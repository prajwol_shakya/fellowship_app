@extends('layouts.app')

@section('content')

<div class="modal fade bd-example-modal-lg" id="myModel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header mymodel-header">
            <h5 class="modal-title " id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body mymodel">
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
      </div>
    </div>
  </div>
  <div class="modal fade bd-example-modal-lg" id="myfile" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header file-model-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body myfile">
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary " data-dismiss="modal">Close</button>
          </div>
      </div>
    </div>
  </div>

<div class="container-fluid">
    <h6>{{ $form->name }}</h6>

    <table class="table table-bordered" id="users-table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Mobile No</th>
                <th>Payment Type</th>
                <th>Payment Status</th>
                <th>Application Status</th>
                <th>Submitted Date</th>
                <th>Action</th>

            </tr>
        </thead>
    </table>
    </div>
</div>
@endsection
@push('style')
<style>
#theImg
{
    height: 450px;
    width: 750px;

}
</style>
@endpush
@push('script')
<script>
$(function() {
    var form_id = {!! $form->id !!};
    let table =   $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/fellowship-form/'+form_id,
        
        columns: [
            { data: 'full_name', name: 'full_name' },
            { data: 'mobile_no', name: 'mobile_no' },
            { data: 'payment_type', name: 'payment_type' },
            { data: 'payment_status', name: 'payment_status' },
            { data: 'application', name: 'application' },
            { data: 'created_at', name: 'created_at' },
            { data: 'action', name: 'action' }
        ],
        "createdRow": function ( row, data, index ) {
            console.log(data['application_status'],'sdfsaf')
            if ( data['application_status'] == 1 ) {
                $('td', row).eq(4).addClass('success');
            } 
            if ( data['payment_approved'] == 1 ) {
                $('td', row).eq(3).addClass('success');
            } 
},
        
    });
    table.on('draw.dt', function () {
        $('.actionBtnTable').change(function(){
            console.log(this.value)
            $('.actionBtnTable').not($(this)).prop('selectedIndex',0);

            if (this.value == 1) {
                var user_id = $(this).find(':selected').data('user_id');
                var form_id = $(this).find(':selected').data('form_id');

                console.log('user',user_id)
                getuserApplication(user_id,form_id);
                }
            else if (this.value == 2) {
               var gateway_ref = $(this).find(':selected').data('gateway_ref');
               var is_image = gateway_ref.split('.');
                    var image = '/voucher/'+gateway_ref;
                    if(is_image[1] == "pdf")
                    {
                    $('.mymodel').empty().prepend($('<iframe>',{id:'theImg',src:image}));
                    $(".mymodel-header > h5").text("View Voucher");
                    }  
                    else
                    {
                    $('.mymodel').empty().prepend($('<img>',{id:'theImg',src:image}));
                    $(".mymodel-header > h5").text("View Voucher");
                    }
                  
                    
                   
                $('#myModel').modal('show');
                }
                
              else if (this.value == 3) {
                var user_id = $(this).find(':selected').data('user_id');
                    approvePay(user_id);
                }
               else if (this.value == 4) {      
                var user_id = $(this).find(':selected').data('user_id');              
                    approveApp(user_id);
                }

            });
            
        });

$( "body" ).click(function() {
 
    if( !$('#myfile').hasClass('show') ) {
        $('#myModel').css('overflow-y', 'auto');

}

});
    function getuserApplication(user_id,form_id)
    {
        $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
                });
                console.log();
                $.ajax({
                    url: '/get-user-application',
                    type: 'POST',
                    dataType: "json",
                    data:{
                         'user_id': user_id,
                         'form_id':form_id
                        },
                    success: function (data) {
                        $('.mymodel').empty().append(data.data);
                        $(".mymodel-header > h5").empty().text("Applicant Form Detail"); 
                        $('#myModel').modal('show');

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // Empty most of the time...
                    }
                });
    }
    function approveApp(user_id)
            {
                Swal.fire({
                title: 'Are you sure you want to approve Application ?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, approved it!'
                }).then((result) => {
                if (result.isConfirmed) {

                    $.ajaxSetup({
                                headers: {
                                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                                }
                                });
                                $.ajax({
                                    url: '/update-appliccation-status',
                                    type: 'POST',
                                    dataType: "json",
                                    data:{
                                        'user_id': user_id,
                                        'form_id' :form_id
                                        },
                                    success: function (data) {
                                    if(data.status === 200) 
                                    {
                                        Swal.fire({
                                            title: 'Application approved and Email sent',
                                            confirmButtonText: `OK`
                                        })
                                        .then((value) => {
                                            window.location.href= "/user-applied-form-list/"+form_id;
                                    });
                                    }
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        // Empty most of the time...
                                    }
                                });
                }
                })
              
            }
    function approvePay(user_id)
            {
                Swal.fire({
                    title: 'Are you sure you want to approve payment ?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, approve it!'
                    }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajaxSetup({
                                    headers: {
                                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                                    }
                                    });
                                    $.ajax({
                                        url: '/update-payment-status',
                                        type: 'POST',
                                        dataType: "json",
                                        data:{
                                            'user_id': user_id
                                            },
                                        success: function (data) {
                                        if(data.status === 200) 
                                        {
                                            Swal.fire({
                                                title: 'Payment approved',
                                                confirmButtonText: `OK`
                                            })
                                            .then((value) => {
                                                window.location.href= "/user-applied-form-list/"+form_id;
                                        });
                                        }
                                        },
                                        error: function (jqXHR, textStatus, errorThrown) {
                                            // Empty most of the time...
                                        }
                                });
                }
                })
              
            }
               
});
</script>
@endpush
