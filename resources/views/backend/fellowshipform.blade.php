<style>
     .view-file
        {
            background-color: #4c96e7 !important;
            border-color: #4c96e7 !important;
        }
        .doc-view
        {
            margin: 10px;
        }
    </style>
<div class="container">
    <center>
        <h3>Personal Detail</h3>
    </center>
    <div class="row">
        <div class="col-sm">
            Name : {{ $app->full_name }}
        </div>
        <div class="col-sm">
            Father Name : {{ $app->father_name }}
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
            Mother Name : {{ $app->mother_name }}
        </div>


        <div class="col-sm">
            Citizenship No : {{ $app->citizenship_no }}
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
            Citizenship Date : {{ $app->citizenship_date }}
        </div>
        <div class="col-sm">
            Citizenship District : {{ $app->citizenship_district }}
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
            Nationality : {{ $app->nationality }}
        </div>
        <div class="col-sm">
            Date of Birth : {{ $app->dob }}
        </div>

    </div>
    <br>
    <center>
        <h3>Contact Detail</h3>
    </center>

    <div class="row">
        <div class="col-sm">
            Email : {{ $app->email }}
        </div>

        <div class="col-sm">
            Mobile No : {{ $app->mobile_no }}
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
            Permanent Address : {{ $app->permanent_address }}
        </div>

        <div class="col-sm">
            Correspondence Address : {{ $app->temporary_address }}
        </div>
    </div>
    <br>
    <center>
        <h3>UG Detail</h3>
    </center>
    <div class="row">
        <div class="col-sm">
            UG Degree : {{ $app->ug_degree }}
        </div>

        <div class="col-sm">
            Degree From : {{ $app->degree_from }}
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
            UG College_name : {{ $app->ug_college_name }}
        </div>

        <div class="col-sm">
            NMC Reg Date : {{ $app->nmc_reg_date }}
        </div>
    </div>
    <div class="row">

        <div class="col-sm">
            NMC Reg No : {{ $app->nmc_reg_no }}
        </div>
        

    </div>
    <div class="row">
        <div class="col-4 form-group doc-view" >
            <button type="button" class="btn btn-secondary view-file" data-form="2">View files</button>
        </div>
    </div>
    <br>
    <center>
        <h3>PG Detail</h3>
    </center>
    <div class="row">
        <div class="col-sm">
            PG Degree : {{ $app->pg_degree }}
        </div>

        <div class="col-sm">
            PG Degree From : {{ $app->pg_degree_from }}
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
            PG College Name : {{ $app->pg_college_name }}
        </div>
        <div class="col-sm">
            PG Marks Obtained : {{ $app->pg_marks_obtained }}
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
            Specialist Qualifying Exam : {{ $app->sq_exam_detail }}
        </div>

    </div>
    <div class="row">
        <div class="col-4 form-group doc-view" >
            <button type="button" class="btn btn-secondary view-file" data-form="3">View files</button>
        </div>
    </div>
    <br>
    <center>
        <h3>Experience Detail</h3>
    </center>
    <div class="row">
        <div class="col-sm">
            Affiliated : {{ $app->affiliated }}
        </div>

        <div class="col-sm">
            Insitute/Hospital : {{ $app->insitute_hospital }}
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
            Position Head : {{ $app->position_head }}
        </div>

        <div class="col-sm">
            Experience : {{ $app->experience }}
        </div>
    </div>
    <div class="row">
        <div class="col-4 form-group doc-view" >
            <button type="button" class="btn btn-secondary view-file" data-form="4">View files</button>
        </div>
    </div>
</div>
<script>
        $(document).ready(function() {
     $( ".view-file" ).click(function() {
                var form_no = $(this).data('form');  
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                console.log();
                $.ajax({
                    url: '/admin-preview-file',
                    type: 'POST',
                    dataType: "json",
                    data: {
                        'form_no': form_no,
                        'user_id':{!! $app->user_id !!}
                    },
                    success: function(data) {
                        let a = data.data;
                        a.forEach(element => 
                        {
                            console.log(element)
                            downloadURI(element.url)

                        }
                         );
                        // $('.myfile').empty().append(data.data);
                        // $(".file-model-header > h5").empty().text("Files"); 
                        // $('#myfile').modal('show');

                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                       
                    }
                });
               
            });

            function downloadURI(uri, name='') 
            {

            var link = document.createElement("a");
            // If you don't know the name or want to use
            // the webserver default set name = ''
            link.setAttribute('download', name);
            link.href = uri;
            document.body.appendChild(link);
            link.click();
            link.remove();
        }
        });
           
    </script>
