


<div>
        @foreach($mocks  as $mock)
        @if(in_array($mock['type'],['png','jpg','jpeg']))
        <img src="{{ $mock['url'] }}" style="width: 100%;min-height: 470px;"/>
        @elseif(in_array($mock['type'],['pdf','docx','doc']))
        <iframe src="{{ $mock['url'] }}" style="width:100%;min-height:640px;"></iframe>
        @else
        <h5>NO Document</h5>
        @endif
        @endforeach
</div>
   
