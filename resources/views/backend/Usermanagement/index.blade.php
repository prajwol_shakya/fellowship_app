@extends('layouts.app')

@section('content')
    <div class="modal fade bd-example-modal-lg" id="myModel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="myform">
                        <div class="form-group">
                            <label for="title">Name</label>
                            <input type="text" name='name' class="form-control" id="name">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control" id="email">
                        </div>
                        <div class="form-group">
                            <select class="form-control"  name="role_id">
                                <option selected="true" disabled="disabled">Select Role</option>
                                @foreach ($roles as $role)
                                    <option
                                        value="{{ $role->id }}">{{ $role->display_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="Password">Password</label>
                            <input type="password" name="password" class="form-control" id="password">
                        </div>
                        <div class="form-group">
                            <label for="Conformpassword">Confirm password</label>
                            <input type="password" name="conformpassword" class="form-control" id="conformpassword">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary store">Save changes</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg" id="editModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="editform">
                        <div class="form-group">
                            <label for="title">Name</label>
                            <input type="text" name='name' class="form-control" id="name">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control" id="email">
                        </div>
                        <div class="form-group">
                            <select class="form-control myrole"  name="role_id" >
                                <option > Select Role</option>

                                @foreach ($roles as $role)
                                    <option
                                        value="{{ $role->id }}">{{ $role->display_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-primary role">Update</button>
                        </div>
                        <div class="form-group">
                            <label for="Password">Password</label>
                            <input type="password" name="passwordd" class="form-control" id="passwordd">
                        </div>
                        <div class="form-group">
                            <label for="Conformpassword">Confirm password</label>
                            <input type="password" name="conformpasswordd" class="form-control" id="conformpasswordd">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary edit">Update Password</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
    <div class="container-fluid">
        <button class="btn btn-primary create">Create User</button>

        <table class="table table-bordered" id="users-table">
            <thead>
                <tr>
                    <th>User Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Action</th>

                </tr>
            </thead>
        </table>
    </div>
    </div>
@endsection
@push('style')
    <style>
        #theImg {
            height: 450px;
            width: 750px;

        }

        label.error {
            color: red;
        }
        .create
        {
            margin-bottom: 1%;
            float: right;
        }
        .role
        {
            float: right;
            margin-right: 3%;
            margin-bottom: 2%;
        }
    </style>
@endpush
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>

    <script>
        $(function() {

            let user_id;
            $('.create').click(function() {
                $('input[name="name"]').val('');
                $('input[name="email"]').val('');
                $('input[name="role_id"]').val('');
                $('input[name="password"]').val('');
                $('input[name="conformpassword"]').val('');
                $('input[name="passwordd"]').val('');
                $('input[name="conformpasswordd"]').val('');

                $('#myModel').modal('show');

            });
            let table = loadtable();

            function loadtable() {
                $("#users-table").DataTable().destroy();
                let table = $('#users-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: '{!!  route('list.users') !!}',

                    columns: [{
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'email',
                            name: 'email'
                        },
                        {
                            data: 'display_name',
                            name: 'display_name'
                        },
                        {
                            data: 'action',
                            name: 'action'
                        }
                    ],


                });
                return table;
            }
            table.on('draw.dt', function() {
                $('.actionBtnTable').change(function() {
                    console.log(this.value)
                    $('.actionBtnTable').not($(this)).prop('selectedIndex', 0);

                    if (this.value == 1) {
                        $('input[name="name"]').val('');
                        $('input[name="email"]').val('');
                        $('.myrole').val('');
                        $('input[name="passwordd"]').val('');
                        $('input[name="conformpasswordd"]').val('');
                        var name = $(this).find(':selected').data('name');
                        var email = $(this).find(':selected').data('email');
                        var role_id = $(this).find(':selected').data('role_id');
                        var id = $(this).find(':selected').data('user_id');
                        user_id = id;
                        $('input[name="name"]').val(name);
                        $('input[name="email"]').val(email);
                        $('.myrole').val(role_id);
                        $('#editModel').modal('show');


                    } else if (this.value == 2) {
                        var id = $(this).find(':selected').data('user_id');
                        Swal.fire({
                            title: 'Are you sure you want to Delete User?',
                            text: "You won't be able to revert this!",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Delete it!'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                userdelete(id);
                            }
                        });

                    }
                });
            });

            function userdelete(id) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: 'user-delete',
                    type: 'POST',
                    dataType: "json",
                    data: {
                        'id': id
                    },
                    success: function(data) {
                        if (data.status === 200) {
                            $('#myModel').modal('hide');
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            })
                            loadtable();
                        }
                        else{
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text:data.error,
                                })
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // Empty most of the time...
                    }
                });
            }

            function validation(v) {
                var status = false;
                // $('fieldset').each(function(){
                    let form_name = '#'+v;
                console.log($(this))
                $(form_name).validate({
                    focusCleanup: true,
                    rules: {
                        name: {
                            required: true,
                        },
                        email: {
                            required: true,
                            email: true,
                        },
                        role_id: {
                            required: true,
                        },
                        password: {
                            required: true,
                            minlength: 5,
                        },

                        conformpassword: {
                            required: true,
                            equalTo: "#password"
                        },
                    },
                    messages: {
                        name: "Enter Title",
                        conformpassword: {
                            required: "Please provide a password",
                            equalTo: " Enter Confirm Password Same as Password"
                        },
                        password: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long"
                        },
                        email: "Please enter a valid email address",
                        role_id: "Please select role",

                    }
                });
                if (!$(form_name).valid()) {
                    status = true;
                }


                // });
                return status;

            }
            function validation2(v) {
                var status = false;
                // $('fieldset').each(function(){
                    let form_name = '#'+v;
                console.log($(this))
                $(form_name).validate({
                    focusCleanup: true,
                    rules: {
                        name: {
                            required: true,
                        },
                        email: {
                            required: true,
                            email: true,
                        },
                        role_id: {
                            required: true,
                        },
                        passwordd: {
                            required: true,
                            minlength: 5,
                        },

                        conformpasswordd: {
                            required: true,
                            equalTo: "#passwordd"
                        },
                    },
                    messages: {
                        name: "Enter Title",
                        conformpasswordd: {
                            required: "Please provide a password",
                            equalTo: " Enter Confirm Password Same as Password"
                        },
                        passwordd: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long"
                        },
                        email: "Please enter a valid email address",
                        role_id: "Please select role",

                    }
                });
                if (!$(form_name).valid()) {
                    status = true;
                }


                // });
                return status;

            }
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })


            $('.store').click(function() {
                if (validation('myform')) {
                    return true;
                }
                // $.ajaxSetup({
                //     headers: {
                //         'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                //     }
                // });
                let data = $('#myform').serialize();
                $.ajax({
                    url: 'create-user',
                    type: 'POST',
                    dataType: "json",
                    data: {
                        'data': data
                    },
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    success: function(data) {
                        if (data.status === 200) {
                            $('#myModel').modal('hide');

                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            })
                            loadtable();
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // Empty most of the time...
                    }
                });
            });


            $('.edit').click(function() {
                if (validation2('editform')) {
                    return true;
                }
                // $.ajaxSetup({
                //     headers: {
                //         'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                //     }
                // });
                let data = $('#editform').serialize();
                $.ajax({
                    url: 'update-user/' + user_id,
                    type: 'POST',
                    dataType: "json",
                    data: {
                        'data': data
                    },
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    success: function(data) {
                        if (data.status === 200) {
                            $('#editModel').modal('hide');
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            })
                            loadtable();

                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // Empty most of the time...
                    }
                });
            });

            $('.role').click(function() {
              
                // $.ajaxSetup({
                //     headers: {
                //         'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                //     }
                // });
                let data = $('#editform').serialize();
                $.ajax({
                    url: 'update-user-role/' + user_id,
                    type: 'POST',
                    dataType: "json",
                    data: {
                        'data': data
                    },
                    headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
                    
                    success: function(data) {
                        if (data.status === 200) {
                            $('#editModel').modal('hide');
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            })
                            loadtable();

                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // Empty most of the time...
                    }
                });
            });
        });

    </script>

@endpush
