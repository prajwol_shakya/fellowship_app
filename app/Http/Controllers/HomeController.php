<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cookie;

use Auth;
use App\User;
use App\FellowshipForm;
use App\Invoice;
use Illuminate\Support\Facades\DB;
use App\Http\Classes\NepaliCalendar;
use App\Http\Classes\TicketNumber;
use App\PaymentTransaction;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\FellowshipDocument;
use App\Form;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['forms','setCookies','checkcode']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function forms()
    {
        $forms = Form::all();
        foreach($forms as $form)
        {
            $form->to = Carbon::parse($form->to)->format('jS F, Y ');
        }
        return view('auth.register',compact('forms'));

    }
    public function checkcode(Request $request,$code)
    {
        $user=User::where('verification_code',$code)->first();
        if($user)
        {
            $user->verification_code='';
            $user->is_active=1;
            $user->save();
            // Auth::login($user);
            $message='Your account has been verified successfully!!';
            return redirect('/login')->with('status',$message);
        
        }else{
            $message='We have sent you a confirmation link in your email!! Please Check Your mail';
            return redirect('/login')->with('warning',$message);
        }
    
    }
    public function formSubmit(Request $request)
    {
       $form = FellowshipForm::where('user_id',Auth::user()->id)->update(['is_submit'=>1]);
       if($form)
       {
           return response()->json([
               'status' => 200
           ]);
         
       }
    }
    public function setCookies(Request $request)
    {
        $slug = $request->slug;
        $current = Carbon::now();
        $current = Carbon::parse($current)->format('Y-m-d');
        $form = DB::table('forms')->where('slug', $slug)->first();
        if($form->to >= $current)
        {
            Cookie::queue('slug', $form->slug, 15);
            return response()->json([
                'status' => 200
            ]);
          
        }
        return response()->json([
            'status' => 401,
            'message' =>'Fellowship form already expired'
        ]);
    
    }
    public function index($form)
    {
        
        $form = Form::where('slug',$form)->first();
        if($form)
        {
            $userdata = DB::table('users as u')
            ->leftJoin('fellowship_forms as fsf', 'u.id', '=', 'fsf.user_id')
            ->select('u.id','u.name','u.email','fsf.user_id','fsf.full_name','fsf.mobile_no','fsf.father_name','fsf.mother_name','fsf.nationality','fsf.citizenship_no','fsf.citizenship_date','fsf.citizenship_district','fsf.dob','fsf.permanent_address','fsf.temporary_address','fsf.ug_degree','fsf.degree_from','fsf.ug_college_name','fsf.nmc_reg_date','fsf.nmc_reg_no','fsf.pg_degree','fsf.pg_degree_from','fsf.pg_college_name','fsf.pg_marks_obtained','fsf.sq_exam_detail','fsf.affiliated','fsf.insitute_hospital','fsf.position_head','fsf.experience','fsf.form_no')
            ->where('u.id',Auth::user()->id)
            ->first();
            $form = $form->slug;
            return view('frontend.home',compact('userdata','form'));
        }
        abort(403);
       
    }
    public function checkform(Request $request)
    {
        $form = DB::table('forms')->where('slug', $request->slug)->first();
        $current = Carbon::now();
        $current = Carbon::parse($current)->format('Y-m-d');
        // dd($form->to >= $current,$form->to,$current);
        if($form->to >= $current)
        {
            $invoice = Invoice::where('user_id',Auth::user()->id)->where('form_id',$form->id)->first();
            // dd($invoice);
            if($invoice)
            {
                return response()->json([
                    'status' => 400
                ]);
            }
            // Cookie::queue('slug', $form->slug, 15);

            return response()->json([
                'status' => 200
            ]);
          
        }
        return response()->json([
            'status' => 401,
            'message' =>'Fellowship form already expired'
        ]);
       

    }
    public function userforms()
    {
        $forms = Form::all();
        foreach($forms as $form)
        {
            $fellowform = FellowshipForm::where('user_id',Auth::user()->id)->where('form_id',$form->id)->first();
            if($fellowform)
            {
                if($fellowform->is_submit)
                {
                    $form['status'] = 1;
                }
                else
                {
                    $form['status'] = 0;
                }
            }
            
            $inv = Invoice::where('user_id',Auth::user()->id)->where('form_id',$form->id)->first();
            if($inv)
            {
                $form['is_applied'] = 0;
            }
            else
            {
                $form['is_applied'] = 1;
            }
            $form->to = Carbon::parse($form->to)->format('jS F, Y ');
        }
        return view('frontend.form',compact('forms'));
    }
    public function preview($form)
    {
        $form = Form::where('slug',$form)->first();
        if($form)
        {
        $invoice = Invoice::where('form_id',$form->id)->where('user_id',Auth()->user()->id)->first();
        $status = 0;
        if($invoice)
        {
            $status = 1;
        }
        $ugdocuments = FellowshipDocument::where('form_id',2)->where('user_id',Auth::user()->id)->get();
        $pgdocuments = FellowshipDocument::where('form_id',3)->where('user_id',Auth::user()->id)->get();
        $exdocuments = FellowshipDocument::where('form_id',4)->where('user_id',Auth::user()->id)->get();

       $userdata = DB::table('users as u')
        ->leftJoin('fellowship_forms as fsf', 'u.id', '=', 'fsf.user_id')
        ->select('u.id','u.name','u.email','fsf.user_id','fsf.full_name','fsf.mobile_no','fsf.father_name','fsf.mother_name','fsf.nationality','fsf.citizenship_no','fsf.citizenship_date','fsf.citizenship_district','fsf.dob','fsf.permanent_address','fsf.temporary_address','fsf.ug_degree','fsf.degree_from','fsf.ug_college_name','fsf.nmc_reg_date','fsf.nmc_reg_no','fsf.pg_degree','fsf.pg_degree_from','fsf.pg_college_name','fsf.pg_marks_obtained','fsf.sq_exam_detail','fsf.affiliated','fsf.insitute_hospital','fsf.position_head','fsf.experience','fsf.form_no')
        ->where('u.id',Auth::user()->id)
        ->first();
        $form = $form->slug;
        return  view('frontend.preview',compact('userdata','form','status','ugdocuments','pgdocuments','exdocuments'));
    }
    abort(403);
       
    }
    public function successPreview($slug)
    {
        // dd($slug);
        $form = Form::where('slug',$slug)->first();
        if($form)
        {
            $invoice = Invoice::where('form_id',$form->id)->where('user_id',Auth()->user()->id)->first();
            $status = 0;
            if($invoice)
            {
                $status = 1;
            }
            $ugdocuments = FellowshipDocument::where('form_id',2)->where('user_id',Auth::user()->id)->get();
            $pgdocuments = FellowshipDocument::where('form_id',3)->where('user_id',Auth::user()->id)->get();
            $exdocuments = FellowshipDocument::where('form_id',4)->where('user_id',Auth::user()->id)->get();
            
       $userdata = DB::table('users as u')
        ->leftJoin('fellowship_forms as fsf', 'u.id', '=', 'fsf.user_id')
        ->select('u.id','u.name','u.email','fsf.user_id','fsf.full_name','fsf.mobile_no','fsf.father_name','fsf.mother_name','fsf.nationality','fsf.citizenship_no','fsf.citizenship_date','fsf.citizenship_district','fsf.dob','fsf.permanent_address','fsf.temporary_address','fsf.ug_degree','fsf.degree_from','fsf.ug_college_name','fsf.nmc_reg_date','fsf.nmc_reg_no','fsf.pg_degree','fsf.pg_degree_from','fsf.pg_college_name','fsf.pg_marks_obtained','fsf.sq_exam_detail','fsf.affiliated','fsf.insitute_hospital','fsf.position_head','fsf.experience','fsf.form_no')
        ->where('u.id',Auth::user()->id)
        ->first();
        $form = $form->slug;
        return  view('frontend.preview',compact('userdata','form','status','ugdocuments','pgdocuments','exdocuments'));
    }
    abort(403);
    }
    public function editFormNo(Request $request)
    {
        $form = FellowshipForm::where('user_id',Auth::user()->id)->update(['form_no' => $request->form_no]);
        if($form)
        {
            return response()->json([
                'status' => 200
            ]);
        }
        return response()->json([
            'status' => 401
        ]);
    }
    public function image(Request $request)
    {
    // $path = storage_path('document');
    $file = $request->file('file');
    // $name = $file->getClientOriginalName();
    $name = time().'_'.$file->getClientOriginalName();
    $document = FellowshipDocument::create([
        'form_id' => $request->form_id,
        'user_id' =>Auth::user()->id,
        'document' => $name,
        'type' => $file->extension()
    ]);
    $file->move(public_path('document'), $name);
    if($document)
    {
        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }
    return response()->json([
        'status' => 401
    ]);
   
    }
    public function documentDestroy(Request $request)
    {
        // dd($request->all());
        $filename = $request->filename;
        FellowshipDocument::where('document',$filename)->where('user_id',Auth::user()->id)->delete();
        $path=public_path().'/document/'.$filename;
        if (file_exists($path)) {
            unlink($path);
        }
        return response()->json([
            'status' => 200
        ]);
    }
    public function getDocument(Request $request)
    { 
        $documents = FellowshipDocument::where('form_id',$request->form_id)->where('user_id',Auth::user()->id)->get();
        // dd($documents);
        $mocks = [];
        if($documents)
        {
            foreach($documents as $doc)
            {
                $mock['name'] = $doc->document;
                $path ='/document/'.$doc->document;
                $mock['url'] = $path;
                $mocks[] = $mock;
            }
        }
        return response()->json([
            'status' => 200,
            'data' => $mocks
        ]);
    }

    public function previewfile(Request $request)
    {   $data = $request->all();
        $form_id = $data['form_no'];
        $documents = FellowshipDocument::where('form_id',$form_id)->where('user_id',Auth::user()->id)->get();
        $mocks = [];
        if($documents)
        {
            foreach($documents as $doc)
            {
                $mock['name'] = $doc->document;
                $path ='/document/'.$doc->document;
                $mock['url'] = $path;
                $mock['type'] = $doc->type;
                $mocks[] = $mock;
            }
        }
        // $data =  view('frontend.previewfile',compact('mocks'))->render();

        return response()->json([
            'status' => 200,
            'data' => $mocks
        ]);
    }
    
   
    public function personalStore(Request $request)
    {
        $params = array();
        $data = array();
        $form_no = '';
        parse_str($request->data, $params);
        foreach($params as $key=>$value)
        {
        $x = $value == "" ? null : $value;
        $data[$key] = $x;
        
        }
        try{

          $form = FellowshipForm::updateOrCreate(
            ['user_id' => auth()->user()->id],$data);

         return response()
                 ->json([
                         'status' => 200,
                         'message' => 'Fellow ship form successfully stored',
                         'errors' => [],
                     ],200);
		} catch (QueryException $e) {
         return response()
                 ->json([
                         'status' => 401,
                         'message' => 'Error while Saving!',
                         'errors' => $e,
                         'data'  => [] 
                     ]);
     	} catch (Exception $e) {
         return response()
                 ->json([
                         'status' => 401,
                         'message' => 'Error while Saving!',
                         'errors' => $e,
                         'data'  => [] 
                     ]);
     	}
    }

    public function payTransaction(Request $request){
       
        $user_id = Auth::id();
        $form = DB::table('forms')
        ->where('slug',$request->slug)
        ->first();
        if(!$form)
        {
            return response()
            ->json([
                    'status' => 401,
                    'message' => 'oops someting went wrong!',
                    'errors' => [],
                
                ]);
        }
        $payment = PaymentTransaction::where('user_id',$user_id)->where('form_id',$form->id)->where('status',400)->first();
        $params = array();
        $data = array();
        parse_str($request->data, $params);
        foreach($params as $key=>$value)
        {
        $x = $value == "" ? null : $value;
        $data[$key] = $x;
        }
        $data['form_id']=$form->id;
        $userform = FellowshipForm::updateOrCreate(
            ['user_id' => $user_id],$data);
    
        if(!$payment)
        {
                // $total = config('custom.form_price');
                $total = $form->amount;
                $fiscalYear = NepaliCalendar::fiscalYear();
                $latest_bill_no = DB::table('payment_transactions')
                                        ->select('inv_id')
                                        ->orderBy('id', 'desc')
                                        ->first();
                                        // dd( $latest_bill_no);
                $payment_type = 2; //online payment
                if(!$latest_bill_no){
                    $invoiceid = 1;
                }else{
                    $invoiceid = TicketNumber::getNextBillNumber($latest_bill_no->inv_id,$fiscalYear);
                }
                $bill_no = TicketNumber::getBillNumber($payment_type, $invoiceid, $fiscalYear);
                $token = Str::random(60);

                $payment = new PaymentTransaction;
                $payment->user_id = $user_id;
                $payment->fellowship_form_id = $userform->id; //user additional info table id
                $payment->total = $total;
                $payment->inv_id = $bill_no;
                $payment->status = 400;
                $payment->token = $token;
                $payment->form_id = $form->id;
                $payment->created_at = Carbon::now();
                
                if($payment->save()){
                    return response()
                    ->json([
                            'status' => 200,
                            'message' => 'Invoice Successfully Added!',
                            'errors' => [],
                        
                        ],200);
                }else{
                    return response()
                    ->json([
                            'status' => 400,
                            'message' => 'Error While Saving!'
                        ],400);
                }
                
        }
        return response()
        ->json([
                'status' => 200,
                'message' => 'Successfully Added!',
                'errors' => [],
            
            ],200);
    }
    public function getUserForm(Request $request)
    {
        $user_id = Auth::user()->id;
        $app = DB::table('fellowship_forms as fsf')
        ->leftJoin('users as u', 'u.id', '=', 'fsf.user_id')
        ->select('u.email','fsf.*')
        ->where('u.id',$user_id)->first();
        $form = Form::where('id',$request->form_id)->first();
       
        $invoice = Invoice::where('form_id',$form->id)->where('user_id',Auth()->user()->id)->first();
        $status = 0;
        if($invoice)
        {
            $status = 1;
        }
        $ugdocuments = FellowshipDocument::where('form_id',2)->where('user_id',Auth::user()->id)->get();
        $pgdocuments = FellowshipDocument::where('form_id',3)->where('user_id',Auth::user()->id)->get();
        $exdocuments = FellowshipDocument::where('form_id',4)->where('user_id',Auth::user()->id)->get();

        $data = view('frontend.formDetail',compact('app','form','status','ugdocuments','pgdocuments','exdocuments'))->render();
        return response()
        ->json([
            'data' => $data,
        ],200);
    }

    

   
}
