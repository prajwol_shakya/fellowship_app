<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Form;
use Auth;
use DataTables;
use Illuminate\Support\Facades\Hash;
use DB;
class UsermanagementController extends Controller
{
    //
    public function index()
    {
        $roles = Role::where('id','!=',3)->get();
        return view('backend.Usermanagement.index',compact('roles'));
    }
    public function create( Request $request)
    {
        $params = array();
        $data = array();
        parse_str($request->data, $params);
        foreach($params as $key=>$value)
        {
        $x = $value == "" ? null : $value;
        $data[$key] = $x;
        
        }
        $data['user_type'] = 1;

        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);
        $user->attachRole($data['role_id']);

        if($user)
        {
            return response()
            ->json([
                    'status' => 200,
                    'message' => 'User successfully stored',
                    'errors' => [],
                ],200);
        }
        else
        {
            return response()
            ->json([
                    'status' => 401,
                    'errors' => 'oops someting went wrong',
                ],200);
        }
       
    }
    public function update( Request $request,$id)
    {
        $params = array();
        $data = array();
        $form_no = '';
        parse_str($request->data, $params);
        foreach($params as $key=>$value)
        {
        $x = $value == "" ? null : $value;
        $data[$key] = $x;
        
        }
        $role_id = $data['role_id'];
        $data['password'] = Hash::make($data['passwordd']);
        unset($data['role_id']);
        unset($data['conformpasswordd']);
        unset($data['passwordd']);


        $user = User::where('id',$id)->first();
        $role = Role::where('id', $role_id)->first();

        $user->syncRoles([$role]);

        $user = $user->update($data);
        // dd($role);
        if($user)
        {
            return response()
            ->json([
                    'status' => 200,
                    'message' => 'User successfully Updated',
                    'errors' => [],
                ],200);
        }
        else
        {
            return response()
            ->json([
                    'status' => 401,
                    'errors' => 'oops someting went wrong',
                ],200);
        }
       
    }
    public function updateRole( Request $request,$id)
    {
        $params = array();
        $data = array();
        $form_no = '';
        parse_str($request->data, $params);
        foreach($params as $key=>$value)
        {
        $x = $value == "" ? null : $value;
        $data[$key] = $x;
        
        }
        $role_id = $data['role_id'];
        unset($data['role_id']);
        unset($data['conformpasswordd']);
        unset($data['passwordd']);
        $user = User::where('id',$id)->first();
        $role = Role::where('id', $role_id)->first();

        $user->syncRoles([$role]);

        $user = $user->update($data);
        // dd($role);
        if($user)
        {
            return response()
            ->json([
                    'status' => 200,
                    'message' => 'User successfully Updated',
                    'errors' => [],
                ],200);
        }
        else
        {
            return response()
            ->json([
                    'status' => 401,
                    'errors' => 'oops someting went wrong',
                ],200);
        }
       
    }
    public function delete( Request $request)
    {
        $form = Form::where('created_by',$request->id)->first();
        if($form)
        {
            return response()
            ->json([
                    'status' => 401,
                    'error' => "you can't delete user",
                ]);
        }
        $user = User::where('id',$request->id)->delete();
        if($user)
        {
            return response()
            ->json([
                    'status' => 200,
                    'message' => 'User successfully Deleted',
                    'errors' => [],
                ],200);
        }
        else
        {
            return response()
            ->json([
                    'status' => 401,
                    'errors' => 'oops someting went wrong',
                ],200);
        }
       
    }
    public function userlist(Request $request)
    {
        $users = DB::table('users as u')
        ->select('u.name','u.email','r.id as role_id','u.id as user_id','r.display_name')
        ->leftjoin('role_user as ru','ru.user_id','=','u.id')
        ->leftjoin('roles as r','r.id','=','ru.role_id')
        ->where('ru.role_id' ,'!=',3);
        return Datatables::of($users)
            ->addColumn('action', function ($user) {
                $btn = '<select class="form-control actionBtnTable">';
                $btn .= "<option selected>Select Action</option>";
                $btn .='<option data-selected="edit" data-name="'.$user->name.'" data-user_id="'.$user->user_id.'" data-email="'.$user->email.'"  data-role_id="'.$user->role_id.'"   value="1"  >Edit</option>';
                $btn .='<option data-selected="delete"  data-user_id="'.$user->user_id.'"  value="2">Delete</option>';
                $btn .= ' </select>';
                 return $btn;
                
            })
      
        ->make(true);
    }
}
