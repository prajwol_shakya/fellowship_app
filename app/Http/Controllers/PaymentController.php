<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\FellowshipForm;
use Illuminate\Support\Facades\DB;
use App\Http\Classes\NepaliCalendar;
use App\Http\Classes\TicketNumber;
use App\PaymentTransaction;
use App\Invoice;
use Carbon\Carbon;
use App\Form;

use DataTables;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    function failure(Request $request){
        return view('frontend.esewaStatus.payfailure');
    }
    function esewaPaySuccessful(Request $request){
        return view('frontend.esewaStatus.success');
    }

    public function payment($form)
    {
        $form = Form::where('slug',$form)->first();
        if($form)
        {
        $pay = PaymentTransaction::where('user_id',auth()->id())->where('form_id',$form->id)->where('status',400)->first();
        $form = $form;
        return view('frontend.payment',compact('pay','form'));
    }
    abort(403);
    }
    public function history()
    {
        return view('frontend.dashboard');
    }
    public function payByBankVoucher(Request $request)
    {
       if($request->file == 'undefined')
       {
       return response()
            ->json([
                'status' => 500,
                'message' => 'Error while Saving!',
                'data'  => [] 
            ]);
       }
        try{
        $user_id = Auth::user()->id;
       $total = $request->total;
       $inv_id = $request->inv_id;
       $data = DB::table('payment_transactions')->where('inv_id', $inv_id)->first();
       $imageName = time().'.'.$request->file->extension(); 
       $payment = DB::table('payment_methods')->where('name', 'Bank Voucher')->first();
       $request->file->move(public_path('voucher'), $imageName);
       DB::beginTransaction();
       $invdata = Invoice::create([
        'invoice_no' => $data->inv_id,
        'user_id' => $user_id,
        'total' => $data->total, 
        'payment_method_id' => $payment->id,
        'fellowship_form_id' => $data->fellowship_form_id,
        'form_id' => $data->form_id,
        'gateway_ref' => $imageName,
        'status' => 200, 
        'gateway_type' => 'Voucher',
        'created_at' => Carbon::now('+5:45')
        ]);
        DB::table('fellowship_forms')->where('user_id', $user_id)->update(['payment_approved' => 0]);
        DB::table('payment_transactions')->where('id', $data->id)->update(['status' => 200, 'payment_method_id' => $payment->id]);
           
        DB::commit();
        return response()
            ->json([
                'status' => 200,
                'message' => 'Invoice Successfully Added!',
                'errors' => [],
            ],200);
            } catch (QueryException $e) {

            DB::rollBack();
            return response()
            ->json([
                'status' => 401,
                'message' => 'Error while Saving!',
                'errors' => $e,
                'data'  => [] 
            ]);
            } catch (Exception $e) {

            DB::rollBack();
            return response()
            ->json([
                'status' => 401,
                'message' => 'Error while Saving!',
                'errors' => $e,
                'data'  => [] 
            ]);
}


       
       
    }

    public function historyList()
    {
        $paymentlist = DB::table('invoices as i')
        ->leftJoin('fellowship_forms as fsf', 'i.user_id', '=', 'fsf.user_id')
        ->leftJoin('forms as f', 'f.id', '=', 'i.form_id')
        ->select('f.id as form_id','fsf.user_id','fsf.full_name','i.invoice_no as inv_id','i.status','fsf.payment_approved','fsf.application_status','fsf.mobile_no','f.name as form_name')
        ->where('i.user_id',Auth::user()->id)
        ->get();

        return Datatables::of($paymentlist)
        ->addColumn('payment_status', function ($pay) {
            if($pay->payment_approved == 0)
            {
               return 'Pending';
            }
            else{
                return 'Approved';
            }
           })
           ->addColumn('app_status', function ($pay) {
            
                if($pay->application_status == 0)
                {
                   return 'Pending';
                }
                else{
                    return 'Approved';
                }

            
           })
           ->setRowAttr([
            'style' => function($form){
                return $form->application_status ==1 ? 'background-color: #5ada9f;' : 'background-color: #ffffff;';
            }
        ])
         ->addColumn('action', function ($pay) {
                return '<a class="btn btn-xs btn-primary detail" data-form_id="'.$pay->form_id.'" id="detail">View Details</a>';
            })
        ->make(true);
    }

    function esewaPay($invoiceid,$status,$gateway_ref,$payid){
        try{
            $user_id = Auth::id();
            $user_email = Auth::user()->email;
            $user_name = Auth::user()->name;
            $data = DB::table('payment_transactions')->where('inv_id', $invoiceid)->first();
			// $quantity = $data->quantity;
            $total = $data->total;
            $pay_transaction_id = $data->id;
            DB::beginTransaction();

			$invdata = Invoice::create([
                        'invoice_no' => $invoiceid,
                        'user_id' => $user_id,
                        'total' => $total, 
                        'payment_method_id' => $payid,
                        'fellowship_form_id' => $data->fellowship_form_id,
                        'form_id' => $data->form_id,
                        'gateway_ref' => $gateway_ref,
                        'status' => $status, 
                        'gateway_type' => 'Esewa',
                        'created_at' => Carbon::now('+5:45')
                        ]);
            DB::table('fellowship_forms')->where('user_id', $user_id)->update(['payment_approved' => 1]);
            DB::table('payment_transactions')->where('id', $pay_transaction_id)->update(['status' => 200, 'payment_method_id' => $payid]);
           
            DB::commit();

            
         return response()
                 ->json([
                         'status' => 200,
                         'message' => 'Invoice Successfully Added!',
                         'errors' => [],
                     ],200);
		} catch (QueryException $e) {
			DB::rollBack();
         return response()
                 ->json([
                         'status' => 401,
                         'message' => 'Error while Saving!',
                         'errors' => $e,
                         'data'  => [] 
                     ]);
     	} catch (Exception $e) {
			DB::rollBack();
         return response()
                 ->json([
                         'status' => 401,
                         'message' => 'Error while Saving!',
                         'errors' => $e,
                         'data'  => [] 
                     ]);
     	}
    }


    function esewaPaymentVerify(Request $request){
        $stat = $request->q;
    if($stat=="su"){
		$invid = $request->oid;
        $refid = $request->refId;
        
        $details = DB::table('payment_transactions')
        ->select('id', 'inv_id', 'total')
        ->where('inv_id', $invid)
        ->first();
        $form_id = $details->form_id;
        
        $amount = $details->total;
		$prodid = $details->inv_id; 
        $url = "https://uat.esewa.com.np/epay/transrec";
		$data =[
			'amt'=> $amount,
			'rid'=> $refid,
			'pid'=> $prodid,
			'scd'=> 'EPAYTEST'
        ];

		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($curl);
        curl_close($curl);
        $msg = preg_split( '/[\r\n]+/', $response ); 

        $payment = DB::table('payment_methods')->where('name', 'Esewa')->first();
        $payment_method_id = $payment->id;
		if($msg[2]=="Success"){
			$gateway_ref = $refid;
			$status = 200;
            $this->esewaPay($prodid,$status,$gateway_ref,$payment_method_id);
            $form = Form::where('id',$form_id)->first();
            return redirect('/success-preview/'.$form->slug)->with('success', 'Success!! Please check your email for tickets');
        }else{
			$gateway_ref = $refid;
            $status = 400;
            DB::table('payment_transactions')->where('inv_id', $invid)->update(['status' => 400, 'payment_method_id' => $payment_method_id]);
            return redirect('/payment_failure')->with('error', 'Amount Mismatch');
        }
    }

   
}


 
   
}
