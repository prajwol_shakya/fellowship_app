<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use DB;
use Cookie;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    protected function authenticated(Request $request, $user)
    {
        if(Auth::user()->user_type == 2){
            $value = Cookie::get('slug');
            // Cookie::forget('slug');
            Cookie::queue('slug', '', 15);
            $url = '/home/'.$value;
            $user_id = Auth::user()->id;
            if(Auth::user()->is_active == 0)
            {
                Auth::logout();
                $message='We have sent you a confirmation link in your email!! Please Check Your mail';
                return redirect('/login')->with('warning',$message);
            }

            $check_invoice = DB::table('forms as f')->leftjoin('invoices as i','i.form_id','=','f.id')->where('i.user_id',$user_id)->where('f.slug', $value)->first();
            if($value && !$check_invoice)
            {
                // Cookie::forget('slug');
                return redirect()->intended($url);
            }
            else
            {
                return redirect()->intended('/user/form-list');

            }

        }else{
           return redirect()->intended('/fellowship-forms-list');
        }
    }
   
}
