<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Role;
use App\FellowshipForm;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Mail\SendEmail;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/user/form-list';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'user_type' => 2,
            'password' => Hash::make($data['password']),
        ]);
        FellowshipForm::create([
            'user_id' => $user->id,
            'full_name' => $data['name'],
            'dob' => $data['dob'],
            'nationality' => $data['nationality']
            ]);
        return $user;
    }
    public function registerUser(Request $request)
    {
      $data = $request->all();
        try{
            $validatedData = $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);
        // $this->validator($request->all())->validate();
        $verification_code=$this->genrationverificationcode();
        DB::beginTransaction();
        $user =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'user_type' => 2,
            'verification_code' => $verification_code,
            'password' => Hash::make($data['password']),
        ]);
        FellowshipForm::create([
            'user_id' => $user->id,
            'full_name' => $data['name'],
            'dob' => $data['dob'],
            'nationality' => $data['nationality']
            ]);
    
        $role = Role::where('name', 'user')->first();
        $user->attachRole($role);
        DB::commit();
        $message='Please follow the <a href="'.config('custom.app_url').'/verifyaccount/'.$user->verification_code.'">link</a> to verify your account.';
        $this->sendmessage('Account Verification',$message,$user);

        return redirect('/login')->with('status', 'We have sent you an activation code. Check your email and click on the link to verify.');
     
        }catch (QueryException $e) {
			DB::rollBack();
            return redirect('/login')->with('warning', 'Error While Saving!.');
     	}
    }

    public function genrationverificationcode()
    {
        $characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
            for ($i = 0; $i < 20; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
        $pin=$randomString;
        $check=User::where('verification_code',$pin)->first();
        if($check)
        {
          $this->genrationverificationcode();
        }
        else{
            return $pin;
        }
    }

    public function sendmessage($subject,$message,$user)
    
    {
        try {
            Mail::to($user->email)->send(new SendEmail($subject, $message,$user));
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }
        
    }
}
