<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Form;
use Auth;
use DataTables;
use Carbon\Carbon;
use DB;
class FellowshipFormController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {

        return view('backend.fellowshipforms.index');
    }
    public function uploadImage(Request $request)
    {
    $file = $request->file('file');
    $name = time().'_'.$file->getClientOriginalName();
    $file->move(public_path('fellowshipimage'), $name);
  
        return response()->json([
            'location'          => asset('fellowshipimage/'.$name)
        ]);
        
   
    }
    public function create( Request $request)
    {
        $params = array();
        $data = array();
        $form_no = '';
        parse_str($request->data, $params);
        foreach($params as $key=>$value)
        {
        $x = $value == "" ? null : $value;
        $data[$key] = $x;
        
        }
        $data['created_by'] = Auth::user()->id;
        $form = Form::create($data);
        if($form)
        {
            return response()
            ->json([
                    'status' => 200,
                    'message' => 'Form successfully stored',
                    'errors' => [],
                ],200);
        }
        else
        {
            return response()
            ->json([
                    'status' => 401,
                    'errors' => 'oops someting went wrong',
                ],200);
        }
       
    }
    public function update( Request $request,$id)
    {
        $params = array();
        $data = array();
        $form_no = '';
        parse_str($request->data, $params);
        foreach($params as $key=>$value)
        {
        $x = $value == "" ? null : $value;
        $data[$key] = $x;
        
        }
        $data['created_by'] = Auth::user()->id;
        $data['email_format'] = $request->email_format;
        $form = Form::where('id',$id)->update($data);
        if($form)
        {
            return response()
            ->json([
                    'status' => 200,
                    'message' => 'Form successfully Updated',
                    'errors' => [],
                ],200);
        }
        else
        {
            return response()
            ->json([
                    'status' => 401,
                    'errors' => 'oops someting went wrong',
                ],200);
        }
       
    }
    public function delete( Request $request)
    {
       
        $form = Form::where('id',$request->id)->delete();
        if($form)
        {
            return response()
            ->json([
                    'status' => 200,
                    'message' => 'Form successfully Deleted',
                    'errors' => [],
                ],200);
        }
        else
        {
            return response()
            ->json([
                    'status' => 401,
                    'errors' => 'oops someting went wrong',
                ],200);
        }
       
    }
    public function formList(Request $request)
    {
      
        $forms = DB::select("select f.name,f.description,f.email_format,f.amount,f.id,f.from,f.to,
        (select count(*) from invoices as i where f.id = i.form_id)  as applicant
        from forms as f where f.deleted_at is NULL");
        // dd($forms);
        return Datatables::of($forms)
            ->editColumn('to', function($form) {
                if($form->to == null)
                {
                    return '';
                }
                return Carbon::parse($form->to)->format('Y-m-d');;
            })
            ->editColumn('from', function($form) {
                if($form->from == null)
                {
                    return '';
                }
            return Carbon::parse($form->from)->format('Y-m-d');;
            })
            ->addColumn('status', function ($form) {
                $status = '';
                $current = Carbon::parse(Carbon::now())->format('Y-m-d');
                $to = Carbon::parse($form->to)->format('Y-m-d');
                $from = Carbon::parse($form->from)->format('Y-m-d');
             
                if($current > $to)
                {
                    $status = 'Expired';
                }
                else if($current >= $from and $current <= $to)
                {
                    $status = 'Active';
                }
                else if ($current < $from)
                {
                    $status = 'Pending ';

                }
                return $status;
                
            })
            ->addColumn('action', function ($form) {
                $btn = '<select class="form-control actionBtnTable">';
                $btn .= "<option selected disabled>Select Action</option>";
                $btn .='<option data-selected="view"  data-id="'.$form->id.'"  value="3">View Applicants</option>';
                $btn .='<option data-selected="edit" data-name="'.$form->name.'" data-id="'.$form->id.'" data-description="'.$form->description.'" data-email_format = "'.htmlentities($form->email_format).'" data-amount="'.$form->amount.' " data-from="'.$form->from.' " data-to="'.$form->to.' "  value="1"  >Form Settings</option>';
                $btn .='<option data-selected="delete"  data-id="'.$form->id.'"  value="2">Delete form</option>';


                $btn .= ' </select>';
                 return $btn;
                
            })
      
        ->make(true);
    }
}
