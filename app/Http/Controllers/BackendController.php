<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Form;
use App\FellowshipDocument;
use App\FellowshipForm;
use App\Invoice;
use App\PaymentTransaction;
use App\Mail\ApproveAppilcation;
use Mail;
use Illuminate\Support\Facades\DB;
// use Yajra\Datatables\Datatables;
use DataTables;
use Carbon\Carbon;
use Log;
class BackendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    // public function dashboard()
    // {
    //     return view('backend.dashboard');
    // }
    public function userAppliedFormList($form_id)
    {
        $form = Form::find($form_id);
        return view('backend.dashboard',compact('form'));
    }


    public function previewfile(Request $request)
    {  
        $documents = FellowshipDocument::where('form_id',$request->form_no)->where('user_id',$request->user_id)->get();
        $mocks = [];
        if($documents)
        {
            foreach($documents as $doc)
            {
                $mock['name'] = $doc->document;
                $path ='/document/'.$doc->document;
                $mock['url'] = $path;
                $mock['type'] = $doc->type;
                $mocks[] = $mock;
            }
        }
        // $data =  view('frontend.previewfile',compact('mocks'))->render();

        return response()->json([
            'status' => 200,
            'data' => $mocks
        ]);
    }
    public function getuserApplication(Request $request)
    {
        $user_id = $request->user_id;
        $app = DB::table('fellowship_forms as fsf')
        ->leftJoin('users as u', 'u.id', '=', 'fsf.user_id')
        ->select('u.email','fsf.*')
        ->where('u.id',$user_id)->first();
        $form = Form::where('id',$request->form_id)->first();
       
        // $invoice = Invoice::where('form_id',$form->id)->where('user_id',$user_id)->first();
        $status = 2;
        
        $ugdocuments = FellowshipDocument::where('form_id',2)->where('user_id',$user_id)->get();
        $pgdocuments = FellowshipDocument::where('form_id',3)->where('user_id',$user_id)->get();
        $exdocuments = FellowshipDocument::where('form_id',4)->where('user_id',$user_id)->get();

        $data = view('frontend.formDetail',compact('app','form','status','ugdocuments','pgdocuments','exdocuments'))->render();
        return response()
        ->json([
            'data' => $data,
        ],200);
    }

    public function fellowshipforms(Request $request,$form_id)
    {
     
        $fellowship =  DB::table('invoices as i')
        ->Join('fellowship_forms as fsf', 'i.fellowship_form_id', '=', 'fsf.id')
        ->Join('users as u', 'u.id', '=', 'i.user_id')
        ->select('i.id as invoice_id','i.form_id','i.invoice_no','i.created_at','i.gateway_ref','i.gateway_type','u.email','u.id as user_id','fsf.mobile_no','fsf.full_name','fsf.nationality','fsf.citizenship_no','i.payment_method_id','i.status','fsf.application_status','fsf.payment_approved')
        ->where('i.status',200)
        ->where('i.form_id',$form_id)
        ->get();
        return Datatables::of($fellowship)
        ->addColumn('payment_type', function ($form) {
           return  $this->paymentType($form->payment_method_id);
           
        })
        ->addColumn('payment_status', function ($form) {
            if($form->payment_approved == 0)
                return 'Pending';
            else
                return "Approved";
         })
         ->addColumn('application', function ($form) {
            if($form->application_status == 0)
                return 'Pending';
            else
                return "Approved";
         })
        ->addColumn('payment_type', function ($form) {
            return  $this->paymentType($form->payment_method_id);
            
         })
         ->editColumn('created_at', function($form) {
                return Carbon::parse($form->created_at)->format('Y-m-d');;
        })
         ->addColumn('action', function ($form) {
                $btn = '<select class="form-control actionBtnTable">';
                $btn .= "<option selected>Select Action</option>";
                $btn .='<option data-selected="view" data-user_id="'.$form->user_id.'" invoice_id="'.$form->invoice_id.'" form_id="'.$form->form_id.'"  value="1"  >View Details</option>';
                if($form->gateway_type =="Voucher")
                 {
                   
                    $btn .='<option data-selected="gateway_ref" data-gateway_ref="'.$form->gateway_ref.'"  value="2" >View Voucher</option>';

                 }
                 if($form->payment_approved == 0)
                 {
                $btn .='<option data-selected="payment" data-user_id="'.$form->user_id.'" data-invoice_id="'.$form->invoice_id.'" value="3">Approve Payment</option>';
                 }
                 if($form->application_status == 0)
                 {
                $btn .='<option data-selected="app" data-user_id="'.$form->user_id.'" data-invoice_id="'.$form->invoice_id.'"  value="4">Approve application</option>';
                 }
                 
                $btn .= ' </select>';
                 return $btn;
                
            })
            // ->setRowAttr([
            //     'style' => function($form){
            //         return $form->application_status ==1 ? 'background-color: #5ada9f;' : 'background-color: #ffffff;';
            //     }
            // ])
        ->make(true);
    }
    public function updatepaymentStatus(Request $request)
    {       
        $data = $request->all();
        $user_id = $data['user_id'];
       $form = FellowshipForm::where('user_id',$user_id)->update(['payment_approved'=>1]);
       if($form)
       {
        return response()
        ->json([
                'status' => 200,
                'message' => 'Successfully updated!',
                'errors' => [],
            ],200);
       }
       return response()
        ->json([
                'status' => 401,
                'message' => 'something went wrong',
                'errors' => [],
            ],200);
    }
    public function updateAppStatus(Request $request)
    {  
        $data = $request->all();
        $user_id = $data['user_id'];
        try {
            DB::beginTransaction();
            $userform = Form::find($request->form_id)->first();
           
            if($userform->email_format)
            {
                $form = FellowshipForm::where('user_id',$user_id)->update(['application_status'=>1,'form_id' =>$userform->id ]);
                Log::info('form '.$userform->id .'of user '.$user_id ."Accepted");
                $user = User::where('id',$user_id)->first();
                $this->sendmessage('BP Koirala FellowShip Team', $userform->email_format,$user);
                DB::commit();
            }
            else
            {
                return response()
                ->json([
                        'status' => 401,
                        'message' => 'Email format empty',
                        'errors' => [],
                    ],200);
            }
            
            return response()
            ->json([
                    'status' => 200,
                    'message' => 'Successfully updated and email sent',
                    'errors' => [],
                ],200);
        }
        catch (\Exception $e) {
            DB::rollBack();
            return response()
            ->json([
                    'status' => 401,
                    'message' => 'something went wrong',
                    'errors' => $e->getMessage(),
                ],200);
        }

       
      
    }


    public function paymentType($type)
    {
        switch ($type) {
            case 1:
              return "Cash";
              break;
            case 2:
                return "Esewa";
                break;
            case 4:
               return "Bank Voucher";
              break;
            default:
              return "pending";
          }
    }

    public function sendmessage($subject,$message,$user)
    
    {
        try {
            Mail::to($user->email)->send(new ApproveAppilcation($subject, $message,$user));
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }
        
    }
   
}
