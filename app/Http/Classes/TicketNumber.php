<?php

namespace App\Http\Classes;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;

class TicketNumber extends Controller
{

    //functions to get invoice and ticket number
    public static function getNextBillNumber($latest_bill_no,$fiscalYear){
        $val = explode('-', $latest_bill_no);
        // dd($val);
        $index = count($val)-1;
        if ((int)(substr($val[$index -1],-4)) < $fiscalYear ) {
            $next_bill_no = 1;
          }else{
            $next_bill_no = ($val[$index])+1;
          }
          return $next_bill_no;
    }

    public static function getBillNumber($payment_type,$next_bill_no, $fiscalYear){
        $partial_bill_no = $fiscalYear.'-'.(sprintf('%07d',$next_bill_no));
            switch ($payment_type) {
                case 1:
                    $bill_no = 'A'.$partial_bill_no;        //  A => Cash Bill
                    break;
    
                case 2:
                    $bill_no = 'B'.$partial_bill_no;        //  B => Online Bill
                    break;
    
                case 4:
                    $bill_no = 'D'.$partial_bill_no;        //  D => Refund Bill
                    break;
                                    
                default:
                    $bill_no = "0";
                    break;
            } 
        return $bill_no;
    }
    
    
    // end fro bill no
    
    
    // for ticket no
    
    
    public static function getYearCode($year){
        $yearcode=[
            "2020"=> "A",
            "2021"=> "B",
            "2022"=> "C",
            "2023"=> "D",
            "2024"=> "E",
            "2025"=> "F",

            "2026"=> "G",
            "2027"=> "H",
            "2028"=> "I",
            "2029"=> "J",
            "2030"=> "K",
            "2031"=> "L",

            "2032"=> "M",
            "2033"=> "N",
            "2034"=> "O",
            "2035"=> "P",
            "2036"=> "Q",
            "2037"=> "R",

            "2038"=> "S",
            "2039"=> "T",
            "2040"=> "U",
            "2041"=> "V",
            "2042"=> "W",
            "2043"=> "X",

            "2044"=> "Y",
            "2045"=> "Z",
        ];

        foreach ($yearcode as $key => $value) {
            if($key == $year){
                return $value;
            }
        }

        return "Error";

    }

    public static function getMonthCode($month){
        $monthcode=[
            "01"=>"A",
            "02"=>"B",
            "03"=>"C",
            "04"=>"D",
            "05"=>"E",
            "06"=>"F",
            "07"=>"G",
            "08"=>"H",
            "09"=>"I",
            "10"=>"J",
            "11"=>"K",
            "12"=>"L",
        ];

        foreach ($monthcode as $key => $value) {
            if($key == $month){
                return $value;
            }
        }

        return "Error";
    }


    public static function lastestTicketNo($patswitch=1){

        $lastcode = DB::table('tickets')
                           ->select('ticket_no')
                           ->orderBy('ticket_no', 'desc')
                           ->first();

         if ($lastcode != null) {
            if (($lastcode->ticket_no) != null) {
               $lastcode = $lastcode->ticket_no;
               $arr = str_split($lastcode, 4);
               return $arr;
            }
        }else{
           $today = Date('Ymd');
           $year = substr($today, 0, 4);
           $month = substr($today, 4, 2);
           $day = substr($today, 6, 2);
           $year = (new static)->getYearCode($year);
           $month = (new static)->getMonthCode($month);
           $today = $year.$month.$day;
           $lastcode = array();
           $lastcode[0] = $today;
           $lastcode[1] = 0;
           $arr = $lastcode;
           return $arr;
       }
   }

   public static function generateTicketCode($lastcode){
    $today = Date('Ymd');
    $year = substr($today, 0, 4);
    $month = substr($today, 4, 2);
    $day = substr($today, 6, 2);

    $year = (new static)->getYearCode($year);
    $month = (new static)->getMonthCode($month);
    
    //see for alternative way
    $today = $year.$month.$day;

        $lastcodeprefix = $lastcode[0];
        $lastcodesuffix = $lastcode[1];
        // dd($lastcodeprefix,$lastcodesuffix);
        if($lastcodeprefix == $today){
            $prefix = $lastcodeprefix;
            $suffix = $lastcodesuffix+1;
            $suffix = str_pad($suffix, 4, "0", STR_PAD_LEFT);//understand
            // dd($prefix.$suffix);
            return $prefix.$suffix;
        } else {
            $prefix = $today;
            $suffix = 0001;
            $suffix = str_pad($suffix, 4, "0", STR_PAD_LEFT);
            // dd($prefix.$suffix,'here');
            return $prefix.$suffix;
        }
    // }
    }
}
