<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FellowshipDocument extends Model
{
    protected $table = 'fellowship_documents';

    protected $fillable = [
       'user_id','form_id','document','type'
    ];
}
