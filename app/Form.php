<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
class Form extends Model
{
    use SoftDeletes, HasSlug;

    protected $table = 'forms';
    protected $fillable = [
        'slug','name','description','amount','created_by','from','to'
       
    ];
    protected $dates = ['deleted_at'];
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom(['name','description'])
            ->saveSlugsTo('slug');
    }
}
