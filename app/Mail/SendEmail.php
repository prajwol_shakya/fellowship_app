<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject;
    protected $message;
    protected $user;
    public function __construct($subject,$message,$user)
    {
   
        $this->message=$message;
        $this->subject=$subject;
        $this->user=$user;
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $message = $this->message;
        $subject = $this->subject;
        $user = $this->user;
        return $this->from($address = 'no-reply@mavorion.com', $name = 'BP Koirala FellowShip Team')
        ->subject($subject)->markdown('auth.emails.send',compact('message','user'));
    }
}
