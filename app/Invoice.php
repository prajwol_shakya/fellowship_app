<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';

    protected $fillable = [
        'invoice_no','user_id','quantity','total','payment_method_id','gateway_ref',
        'status','created_by' ,'gateway_type' ,'fellowship_form_id','form_id'
    ];
}
