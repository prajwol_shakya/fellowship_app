<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FellowshipForm extends Model
{
    protected $fillable = [
        'user_id',
        'mobile_no',
        'father_name',
        'mother_name',
        'nationality',
        'citizenship_no',
        'citizenship_date',
        'citizenship_district',
        'dob',
        'permanent_address',
        'temporary_address',
        'ug_degree',
        'degree_from',
        'ug_college_name',
        'nmc_reg_date',
        'nmc_reg_no',
        'pg_degree',
        'pg_degree_from',
        'pg_college_name',
        'pg_marks_obtained',
        'sq_exam_detail',
        'affiliated',
        'insitute_hospital',
        'position_head',
        'experience',
        'form_no',
        'full_name',
        'payment_approved',
        'application_status',
        'form_id',
        'is_submit'
    ];
}
